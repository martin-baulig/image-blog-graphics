{-# OPTIONS_GHC -Wno-dodgy-exports #-}

module ImageBlog.Graphics.Vips
    ( initializeVips
    , testVips
    , module ImageBlog.Graphics.Vips.Orphans
    , module ImageBlog.Graphics.Vips.VipsException
    , module ImageBlog.Graphics.Vips.VipsImage
    , module ImageBlog.Graphics.Vips.VipsTypes
    ) where

import           Baulig.Prelude

import           ImageBlog.Graphics.Vips.Orphans       ()
import           ImageBlog.Graphics.Vips.VipsException
import           ImageBlog.Graphics.Vips.VipsImage
import           ImageBlog.Graphics.Vips.VipsTypes

foreign import ccall "haskell-vips.h haskell_vips_initialize" haskell_vips_initialize
        :: IO ()

initializeVips :: MonadIO m => m ()
initializeVips = liftIO haskell_vips_initialize

foreign import ccall "haskell-vips.h haskell_vips_test" haskell_vips_test
        :: IO ()

testVips :: IO ()
testVips = haskell_vips_test
