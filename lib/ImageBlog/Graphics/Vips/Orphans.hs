{-# OPTIONS_GHC -Wno-orphans #-}

module ImageBlog.Graphics.Vips.Orphans () where

import           Baulig.Prelude

import           ImageBlog.Graphics.Vips.VipsTypes

-----------------------------------------------------------------------------
-- Enum helpers
-----------------------------------------------------------------------------

imageTypeToText :: ImageType -> Text
imageTypeToText imageType
    | imageType == imageTypeJpeg = "jpeg"
    | imageType == imageTypePng = "png"
    | imageType == imageTypeWebP = "webp"
    | imageType == imageTypeTiff = "tiff"
    | imageType == imageTypeGif = "gif"
    | imageType == imageTypeSvg = "svg"
    | imageType == imageTypeHeif = "heif"
    | imageType == imageTypePdf = "pdf"
    | imageType == imageTypeMagick = "magick"
    | imageType == imageTypeOpenSlide = "openslide"
    | imageType == imageTypePpm = "ppm"
    | imageType == imageTypeFits = "fits"
    | imageType == imageTypeVips = "vips"
    | imageType == imageTypeRaw = "raw"
    | imageType == imageTypeUnknown = "unknown"
    | imageType == imageTypeMissing = "missing"
    | otherwise = "unknown"

instance Display ImageType where
    display this = display $ this <:> imageTypeToText this

instance Show ImageType where
    show = unpack . display
