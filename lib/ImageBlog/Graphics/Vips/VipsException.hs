module ImageBlog.Graphics.Vips.VipsException (VipsException(..)) where

import           Baulig.Prelude

data VipsException = VipsException !(Maybe Text) !Text
    deriving stock (Show, Typeable)

instance Display VipsException where
    display this
        | VipsException (Just error') text
          <- this = display $ this <:> text <+> error'
        | VipsException Nothing text <- this = display $ this <:> text

instance Exception VipsException
