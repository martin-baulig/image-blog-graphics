{-# LANGUAGE ViewPatterns #-}

{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

module ImageBlog.Graphics.Vips.VipsImage
    ( VipsImage
    , VipsImageMetadata(..)
    , VipsRectangle(..)
    , VipsOffset(..)
    , VipsImageFormat(..)
      -- ImageSize
    , ImageSize(..)
    , HasImageSize(..)
      -- VipsImage
    , withVipsImageFromBuffer
    , vipsImageFromBuffer
    , vipsGetImageSize
    , vipsGetImageMetadata
    , vipsCropImage
    , vipsWriteToMemory
    , vipsInsertImage
    , vipsCompositeImage
    , vipsResizeImage
    , vipsResizeImageCopy
    , vipsImageHasAlpha
    , vipsImageGetBands
    , vipsImageAddAlpha
    , vipsSaveAsWebp
    , vipsSaveAsAvif
    , vipsThumbnailBuffer
      -- Misc
    , imageTypeToText
    ) where

import           Baulig.Prelude

import           Foreign
import           Foreign.C.String
import           Foreign.C.Types

import           Control.Monad.Extra
import           Control.Monad.Trans.Resource

import qualified Data.ByteString                       as BS

import           ImageBlog.Graphics.Vips.Orphans       ()
import           ImageBlog.Graphics.Vips.VipsException
import           ImageBlog.Graphics.Vips.VipsTypes

-----------------------------------------------------------------------------
-- VipsImage
-----------------------------------------------------------------------------

data RawImage

data VipsImage = VipsImage !ReleaseKey !(IORef (Maybe (ForeignPtr RawImage)))

data VipsImageFormat =
    VipsImageFormatJpeg | VipsImageFormatPng | VipsImageFormatTiff
    deriving stock (Eq, Ord, Show, Enum)

foreign import ccall unsafe "haskell-vips.h haskell_vips_new_from_buffer" c_new_from_buffer
        :: Ptr CChar -> Int32 -> IO (Ptr RawImage)

foreign import ccall unsafe "haskell-vips.h &haskell_vips_free_image" c_free_image
        :: FunPtr (Ptr RawImage -> IO ())

foreign import ccall unsafe "haskell-vips.h haskell_vips_free_buffer" c_free_buffer
        :: Ptr CChar -> IO ()

foreign import ccall unsafe "haskell-vips.h haskell_vips_get_image_size" c_get_image_size
        :: Ptr RawImage -> Ptr Word32 -> Ptr Word32 -> IO ()

foreign import ccall unsafe "haskell-vips.h haskell_vips_write_to_memory" c_write_to_memory
        :: Ptr RawImage -> Ptr (Ptr CChar) -> Ptr Int32 -> IO CInt

foreign import ccall unsafe "haskell-vips.h haskell_vips_error_buffer" c_get_error_buffer
        :: IO CString

withVipsImageFromBuffer :: (MonadResource m, MonadUnliftIO m)
                        => ByteString
                        -> (VipsImage -> m x)
                        -> m x
withVipsImageFromBuffer binary =
    bracket (vipsImageFromBuffer binary) (\(VipsImage rk _) -> release rk)

vipsImageFromBuffer :: MonadResource m => ByteString -> m VipsImage
vipsImageFromBuffer binary = vipsImageFromPtr $ BS.useAsCStringLen binary
    $ \(c_str, c_len) -> c_new_from_buffer c_str (fromIntegral c_len)

vipsWriteToMemory :: MonadIO m => VipsImage -> m ByteString
vipsWriteToMemory image = vipsInternalWriteToMemory image
    $ \imagePtr outPtr sizePtr -> c_write_to_memory imagePtr outPtr sizePtr

getError :: MonadIO m => Text -> m VipsException
getError message = do
    error' <- liftIO $ c_get_error_buffer >>= peekCString
    pure $ VipsException (Just $ pack error') message

-----------------------------------------------------------------------------
-- Internal API
-----------------------------------------------------------------------------

vipsImageFromPtr :: MonadResource m => IO (Ptr RawImage) -> m VipsImage
vipsImageFromPtr func = do
    raw <- liftIO func
    (rk, ptr) <- allocate (create raw) checkedFree
    pure $ VipsImage rk ptr
  where
    create raw
        | raw == nullPtr =
            throwIO $ VipsException Nothing "Failed to load image from buffer."
        | otherwise = do
            ptr <- newForeignPtr c_free_image raw
            newIORef $ Just ptr

    checkedFree :: IORef (Maybe (ForeignPtr RawImage)) -> IO ()
    checkedFree var = do
        maybePtr <- atomicModifyIORef var (Nothing, )
        whenJust maybePtr finalizeForeignPtr

withInternalImage
    :: MonadIO m => VipsImage -> (ForeignPtr RawImage -> IO x) -> m x
withInternalImage (VipsImage _ var) func =
    liftIO $ bracket takePtr releasePtr func
  where
    takePtr = readIORef var >>= \case
        Just ptr -> pure ptr
        Nothing -> throwM
            $ VipsException Nothing "VipsImage has already been released."

    releasePtr = writeIORef var . Just

withInternalPtr :: MonadIO m => (Ptr RawImage -> IO x) -> VipsImage -> m x
withInternalPtr func image =
    withInternalImage image $ \ptr -> liftIO $ withForeignPtr ptr func

withInternalPtr' :: MonadIO m => VipsImage -> (Ptr RawImage -> IO x) -> m x
withInternalPtr' image func = withInternalPtr func image

vipsInternalWriteToMemory
    :: MonadIO m
    => VipsImage
    -> (Ptr RawImage -> Ptr (Ptr CChar) -> Ptr Int32 -> IO CInt)
    -> m ByteString
vipsInternalWriteToMemory image func =
    withInternalPtr' image $ \imagePtr -> alloca $ \outPtr -> do
        alloca $ \sizePtr -> do
            ret <- func imagePtr outPtr sizePtr
            case ret of
                0 -> do
                    out <- peek outPtr
                    size <- peek sizePtr
                    bs <- BS.packCStringLen (out, fromIntegral size)
                    c_free_buffer out
                    pure bs
                _ -> throwIO =<< getError "Failed to save image."

-----------------------------------------------------------------------------
-- ImageSize
-----------------------------------------------------------------------------

class HasImageSize x where
    imageSize :: x -> ImageSize

    imageWidth :: x -> Word32
    imageWidth this
        | ImageSize w _ <- imageSize this = w

    imageHeight :: x -> Word32
    imageHeight this
        | ImageSize _ h <- imageSize this = h

-----------------------------------------------------------------------------
-- VipsImageSize
-----------------------------------------------------------------------------

data ImageSize = ImageSize !Word32 !Word32
    deriving stock (Eq, Ord, Show)

instance HasImageSize ImageSize where
    imageSize = id

instance Display ImageSize where
    display this = display $ this <:!> imageWidth this <+!> imageHeight this

vipsGetImageSize :: MonadIO m => VipsImage -> m ImageSize
vipsGetImageSize = withInternalPtr
    $ \imagePtr -> alloca $ \widthPtr -> alloca $ \heightPtr -> do
        c_get_image_size imagePtr widthPtr heightPtr
        ImageSize <$> peek widthPtr <*> peek heightPtr

-----------------------------------------------------------------------------
-- VipsImageMetadata
-----------------------------------------------------------------------------

data VipsImageMetadata =
    VipsImageMetadata { vipsImageMetadataFormat  :: !ImageType
                      , vipsImageMetadataWidth   :: !Word32
                      , vipsImageMetadataHeight  :: !Word32
                      , vipsImageMetadataDensity :: !Word32
                      }
    deriving stock (Eq, Show)

instance HasImageSize VipsImageMetadata where
    imageSize this =
        ImageSize (vipsImageMetadataWidth this) (vipsImageMetadataHeight this)

instance Display VipsImageMetadata where
    display this = display $ this <:!> width <+!> height <+!> density <+> format
      where
        VipsImageMetadata width height density format = this

foreign import ccall unsafe "haskell-vips.h haskell_vips_get_image_metadata" c_get_image_metadata
        :: Ptr RawImage
        -> Ptr Word32
        -> Ptr Word32
        -> Ptr Word32
        -> Ptr Word32
        -> IO ()

imageTypeToText :: ImageType -> Text
imageTypeToText imageType
    | imageType == imageTypeJpeg = "jpeg"
    | imageType == imageTypePng = "png"
    | imageType == imageTypeWebP = "webp"
    | imageType == imageTypeTiff = "tiff"
    | imageType == imageTypeGif = "gif"
    | imageType == imageTypeSvg = "svg"
    | imageType == imageTypeHeif = "heif"
    | imageType == imageTypePdf = "pdf"
    | imageType == imageTypeMagick = "magick"
    | imageType == imageTypeOpenSlide = "openslide"
    | imageType == imageTypePpm = "ppm"
    | imageType == imageTypeFits = "fits"
    | imageType == imageTypeVips = "vips"
    | imageType == imageTypeRaw = "raw"
    | imageType == imageTypeUnknown = "unknown"
    | imageType == imageTypeMissing = "missing"
    | otherwise = "unknown"

vipsGetImageMetadata :: MonadIO m => VipsImage -> m VipsImageMetadata
vipsGetImageMetadata = withInternalPtr $ \imagePtr -> alloca $ \widthPtr ->
    alloca $ \heightPtr -> alloca $ \densityPtr -> alloca $ \typePtr -> do
        c_get_image_metadata imagePtr widthPtr heightPtr densityPtr typePtr
        VipsImageMetadata . ImageType <$> peek typePtr <*> peek widthPtr
            <*> peek heightPtr <*> peek densityPtr

-----------------------------------------------------------------------------
-- vipsCropImage
-----------------------------------------------------------------------------

newtype VipsRectangle = VipsRectangle (Int, Int, Int, Int)
    deriving stock (Eq, Ord, Show)

instance Display VipsRectangle where
    display this
        | VipsRectangle (x, y, w, h)
          <- this = display $ this <:!> x <+!> y <+!> w <+!> h

foreign import ccall unsafe "haskell-vips.h haskell_vips_crop_image" c_crop_image
        :: Ptr RawImage -> Int32 -> Int32 -> Int32 -> Int32 -> IO CInt

vipsCropImage :: MonadIO m => VipsImage -> VipsRectangle -> m ()
vipsCropImage image (VipsRectangle (x, y, w, h)) = withInternalPtr' image
    $ \imagePtr -> c_crop_image imagePtr
                                (fromIntegral x)
                                (fromIntegral y)
                                (fromIntegral w)
                                (fromIntegral h) >>= \case
        0 -> pure ()
        _ -> throwIO =<< getError "Failed to crop image."

-----------------------------------------------------------------------------
-- vipsInsertImage
-----------------------------------------------------------------------------

newtype VipsOffset = VipsOffset (Int, Int)
    deriving stock (Eq, Ord, Show)

instance Display VipsOffset where
    display this
        | VipsOffset (x, y) <- this = display $ this <:!> x <+!> y

foreign import ccall unsafe "haskell-vips.h haskell_vips_insert_image" c_insert_image
        :: Ptr RawImage -> Ptr RawImage -> Int32 -> Int32 -> IO CInt

vipsInsertImage :: MonadIO m => VipsImage -> VipsImage -> VipsOffset -> m ()
vipsInsertImage main overlay (VipsOffset (x, y)) = withInternalPtr' main
    $ \mainPtr -> flip withInternalPtr overlay $ \overlayPtr ->
    c_insert_image mainPtr overlayPtr (fromIntegral x) (fromIntegral y)
    >>= \case
        0 -> pure ()
        _ -> throwIO =<< getError "Failed to insert image."

-----------------------------------------------------------------------------
-- vipsCompositeImage
-----------------------------------------------------------------------------

foreign import ccall unsafe "haskell-vips.h haskell_vips_composite" c_composite_image
        :: Ptr RawImage
        -> Ptr RawImage
        -> BlendMode
        -> Int32
        -> Int32
        -> IO CInt

vipsCompositeImage
    :: MonadIO m => VipsImage -> VipsImage -> BlendMode -> VipsOffset -> m ()
vipsCompositeImage main overlay blend (VipsOffset (x, y)) =
    withInternalPtr' main $ \mainPtr -> withInternalPtr' overlay
    $ \overlayPtr -> c_composite_image mainPtr
                                       overlayPtr
                                       blend
                                       (fromIntegral x)
                                       (fromIntegral y) >>= \case
        0 -> pure ()
        _ -> throwIO =<< getError "Failed to composite image."

-----------------------------------------------------------------------------
-- Alpha and Bands
-----------------------------------------------------------------------------

foreign import ccall unsafe "haskell-vips.h haskell_vips_image_has_alpha" c_has_alpha
        :: Ptr RawImage -> IO CBool

foreign import ccall unsafe "haskell-vips.h haskell_vips_image_get_bands" c_get_bands
        :: Ptr RawImage -> IO CInt

foreign import ccall unsafe "haskell-vips.h haskell_vips_add_alpha" c_add_alpha
        :: Ptr RawImage -> IO CInt

vipsImageHasAlpha :: MonadIO m => VipsImage -> m Bool
vipsImageHasAlpha = withInternalPtr (fmap toBool . c_has_alpha)

vipsImageGetBands :: MonadIO m => VipsImage -> m Int
vipsImageGetBands = withInternalPtr $ fmap fromIntegral . c_get_bands

vipsImageAddAlpha :: MonadIO m => VipsImage -> m ()
vipsImageAddAlpha image = do
    withInternalPtr c_add_alpha image >>= \case
        0 -> pure ()
        _ -> throwIO =<< getError "Failed to add alpha channel."

-----------------------------------------------------------------------------
-- vipsResizeImage
-----------------------------------------------------------------------------

foreign import ccall unsafe "haskell-vips.h haskell_vips_resize" c_resize_image
        :: Ptr RawImage -> CDouble -> IO CInt

foreign import ccall unsafe "haskell-vips.h haskell_vips_resize_copy" c_resize_copy
        :: Ptr RawImage -> CDouble -> Ptr (Ptr RawImage) -> IO CInt

vipsResizeImage :: MonadIO m => VipsImage -> Double -> m ()
vipsResizeImage image scaleFactor =
    withInternalPtr' image (`c_resize_image` realToFrac scaleFactor) >>= \case
        0 -> pure ()
        _ -> throwIO =<< getError "Failed to resize image."

vipsResizeImageCopy
    :: (MonadResource m, MonadUnliftIO m) => VipsImage -> Double -> m VipsImage
vipsResizeImageCopy image (realToFrac -> scaleFactor) = withRunInIO $ \io -> do
    withInternalPtr' image $ \imagePtr -> alloca $ \ptr -> do
        io $ vipsImageFromPtr $ c_resize_copy imagePtr scaleFactor ptr >>= \case
            0 -> peek ptr
            _ -> throwIO =<< getError "Failed to resize image."

-----------------------------------------------------------------------------
-- vipsSaveWebP
-----------------------------------------------------------------------------

foreign import ccall unsafe "haskell-vips.h haskell_vips_webpsave" c_webpsave
        :: Ptr RawImage -> Int32 -> Ptr (Ptr CChar) -> Ptr Int32 -> IO CInt

vipsSaveAsWebp :: MonadIO m => VipsImage -> Word8 -> m ByteString
vipsSaveAsWebp image quality =
    vipsInternalWriteToMemory image $ \imagePtr outPtr sizePtr ->
    c_webpsave imagePtr (fromIntegral quality) outPtr sizePtr

-----------------------------------------------------------------------------
-- vipsSaveAvif
-----------------------------------------------------------------------------

foreign import ccall unsafe "haskell-vips.h haskell_vips_avifsave" c_avifsave
        :: Ptr RawImage -> Int32 -> Ptr (Ptr CChar) -> Ptr Int32 -> IO CInt

vipsSaveAsAvif :: MonadIO m => VipsImage -> Word8 -> m ByteString
vipsSaveAsAvif image quality =
    vipsInternalWriteToMemory image $ \imagePtr outPtr sizePtr ->
    c_avifsave imagePtr (fromIntegral quality) outPtr sizePtr

-----------------------------------------------------------------------------
-- vipsThumbnail
-----------------------------------------------------------------------------

foreign import ccall unsafe "haskell-vips.h haskell_vips_thumbnail_buffer" c_thumbnail_buffer
        :: Ptr CChar -> Int32 -> Word32 -> IO (Ptr RawImage)

vipsThumbnailBuffer :: MonadResource m => ByteString -> Word32 -> m VipsImage
vipsThumbnailBuffer binary width = vipsImageFromPtr $ BS.useAsCStringLen binary
    $ \(c_str, c_len) -> c_thumbnail_buffer c_str (fromIntegral c_len) width
