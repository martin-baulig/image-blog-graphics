{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module ImageBlog.Graphics.Vips.VipsTypes where

import Baulig.Prelude

#include <haskell-vips.h>

newtype ImageType = ImageType Word32
    deriving stock (Eq)

#{enum ImageType, ImageType
 , imageTypeJpeg       = IMAGE_TYPE_JPEG
 , imageTypePng        = IMAGE_TYPE_PNG
 , imageTypeWebP       = IMAGE_TYPE_WEBP
 , imageTypeTiff       = IMAGE_TYPE_TIFF
 , imageTypeGif        = IMAGE_TYPE_GIF
 , imageTypeSvg        = IMAGE_TYPE_SVG
 , imageTypeHeif       = IMAGE_TYPE_HEIF
 , imageTypePdf        = IMAGE_TYPE_PDF
 , imageTypeMagick     = IMAGE_TYPE_MAGICK
 , imageTypeOpenSlide  = IMAGE_TYPE_OPENSLIDE
 , imageTypePpm        = IMAGE_TYPE_PPM
 , imageTypeFits       = IMAGE_TYPE_FITS
 , imageTypeVips       = IMAGE_TYPE_VIPS
 , imageTypeRaw        = IMAGE_TYPE_RAW
 , imageTypeUnknown    = IMAGE_TYPE_UNKNOWN
 , imageTypeMissing    = IMAGE_TYPE_MISSING
}

newtype BlendMode = BlendMode Int32
    deriving stock (Eq, Show)

#{enum BlendMode, BlendMode
  , blendModeClear       = VIPS_BLEND_MODE_CLEAR
  , blendModeSource      = VIPS_BLEND_MODE_SOURCE
  , blendModeOver        = VIPS_BLEND_MODE_OVER
  , blendModeIn          = VIPS_BLEND_MODE_IN
  , blendModeOut         = VIPS_BLEND_MODE_OUT
  , blendModeAtop        = VIPS_BLEND_MODE_ATOP
  , blendModeDest        = VIPS_BLEND_MODE_DEST
  , blendModeDestOver    = VIPS_BLEND_MODE_DEST_OVER
  , blendModeDestIn      = VIPS_BLEND_MODE_DEST_IN
  , blendModeDestOut     = VIPS_BLEND_MODE_DEST_OUT
  , blendModeDestAtop    = VIPS_BLEND_MODE_DEST_ATOP
  , bledModeXor          = VIPS_BLEND_MODE_XOR
  , blendModeAdd         = VIPS_BLEND_MODE_ADD
  , blendModeSaturate    = VIPS_BLEND_MODE_SATURATE
  , blendModeMultiply    = VIPS_BLEND_MODE_MULTIPLY
  , blendModeScreen      = VIPS_BLEND_MODE_SCREEN
  , blendModeOverlay     = VIPS_BLEND_MODE_OVERLAY
  , blendModeDarken      = VIPS_BLEND_MODE_DARKEN
  , blendModeLighten     = VIPS_BLEND_MODE_LIGHTEN
  , blendModeColourDodge = VIPS_BLEND_MODE_COLOUR_DODGE
  , blendModeColourBurn  = VIPS_BLEND_MODE_COLOUR_BURN
  , blendModeHardLight   = VIPS_BLEND_MODE_HARD_LIGHT
  , blendModeSoftLight   = VIPS_BLEND_MODE_SOFT_LIGHT
  , blendModeDifference  = VIPS_BLEND_MODE_DIFFERENCE
  , blendModeExclusion   = VIPS_BLEND_MODE_EXCLUSION
}
