#include <haskell-vips.h>

#define HASKELL_VIPS_LOCKING 1

#if DEBUG_RELOC
#include <ctype.h>
#endif

#if HASKELL_VIPS_LOCKING
static GMutex lock_mutex;
#endif

void vips_test (void);

void
haskell_vips_test (void)
{
#if DEBUG_RELOC
    fprintf (stderr, "HELLO FROM NATIVE CODE: %c\n", toupper ('t'));
#endif
}

const char *
haskell_vips_error_buffer (void)
{
    return vips_error_buffer ();
}

static void *
init_once (void *ptr) {
    vips_init ("haskell-sharp");
    return NULL;
}


static void
_haskell_vips_init (void) {
    static GOnce haskell_vips_init_once = G_ONCE_INIT;
    g_once (&haskell_vips_init_once, init_once, NULL);
}


static int
ends_with (const char *str, const char *suffix)
{
    if (!str || !suffix)
        return 0;
    size_t lenstr = strlen (str);
    size_t lensuffix = strlen (suffix);
    if (lensuffix >  lenstr)
        return 0;
    return strncmp (str + lenstr - lensuffix, suffix, lensuffix) == 0;
}

static ImageType
image_type_from_loader (const char *loader)
{
    if (ends_with (loader, "JpegFile") || ends_with (loader, "JpegBuffer")) {
        return IMAGE_TYPE_JPEG;
    } else if (ends_with (loader, "PngFile") || ends_with (loader, "PngBuffer")) {
        return IMAGE_TYPE_PNG;
    } else if (ends_with (loader, "WebpFile")) {
        return IMAGE_TYPE_WEBP;
    } else if (ends_with (loader, "Openslide")) {
        return IMAGE_TYPE_OPENSLIDE;
    } else if (ends_with (loader, "TiffFile") || ends_with (loader, "TiffBuffer")) {
        return IMAGE_TYPE_TIFF;
    } else if (ends_with (loader, "GifFile")) {
        return IMAGE_TYPE_GIF;
    } else if (ends_with (loader, "SvgFile")) {
        return IMAGE_TYPE_SVG;
    } else if (ends_with (loader, "HeifFile")) {
        return IMAGE_TYPE_HEIF;
    } else if (ends_with (loader, "PdfFile")) {
        return IMAGE_TYPE_PDF;
    } else if (ends_with (loader, "Ppm")) {
        return IMAGE_TYPE_PPM;
    } else if (ends_with (loader, "Fits")) {
        return IMAGE_TYPE_FITS;
    } else if (ends_with (loader, "Vips")) {
        return IMAGE_TYPE_VIPS;
    } else if (ends_with (loader, "Magick") || ends_with (loader, "MagickFile")) {
        return IMAGE_TYPE_MAGICK;
    }

    g_warning (G_STRLOC ": %s: unknown loader type: %s", G_STRFUNC, loader);

    return IMAGE_TYPE_UNKNOWN;
}

const char *
get_image_type_id (ImageType imageType) {
    switch (imageType) {
    case IMAGE_TYPE_JPEG: return "jpeg";
    case IMAGE_TYPE_PNG: return "png";
    case IMAGE_TYPE_WEBP: return "webp";
    case IMAGE_TYPE_TIFF: return "tiff";
    case IMAGE_TYPE_GIF: return "gif";
    case IMAGE_TYPE_SVG: return "svg";
    case IMAGE_TYPE_HEIF: return "heif";
    case IMAGE_TYPE_PDF: return "pdf";
    case IMAGE_TYPE_MAGICK: return "magick";
    case IMAGE_TYPE_OPENSLIDE: return "openslide";
    case IMAGE_TYPE_PPM: return "ppm";
    case IMAGE_TYPE_FITS: return "fits";
    case IMAGE_TYPE_VIPS: return "vips";
    case IMAGE_TYPE_RAW: return "raw";
    case IMAGE_TYPE_UNKNOWN: return "unknown";
    case IMAGE_TYPE_MISSING: return "missing";
    }
    return "unknown";
}

const char *
get_image_type_suffix (ImageType imageType) {
    switch (imageType) {
    case IMAGE_TYPE_JPEG: return ".jpeg";
    case IMAGE_TYPE_PNG: return ".png";
    case IMAGE_TYPE_TIFF: return ".tiff";
    default:
        g_warning (G_STRLOC ": %s: unknown image type: %d", G_STRFUNC, imageType);
        return ".bin";
    }
}


HaskellVipsImage *
haskell_vips_open_image (const char *file)
{
    HaskellVipsImage *haskell_image = NULL;
    VipsImage *image;
    const char *loader;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %s", G_STRFUNC, file);
#endif

#if HASKELL_VIPS_LOCKING
    g_mutex_lock (&lock_mutex);
#endif

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s start: %s", G_STRFUNC, file);
#endif

    loader = vips_foreign_find_load (file);
    if (!loader)
        goto out;

    image = vips_image_new_from_file (file, "access", VIPS_ACCESS_RANDOM, NULL);
    if (!image)
        goto out;

    haskell_image = g_new0 (HaskellVipsImage, 1);
    haskell_image->image = image;
    haskell_image->type = image_type_from_loader (loader);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s done: %p - %p - %s", G_STRFUNC, (void*)haskell_image, (void*)image,
               get_image_type_suffix (haskell_image->type));
#endif

 out:
#if HASKELL_VIPS_LOCKING
    g_mutex_unlock (&lock_mutex);
#endif
    return haskell_image;
}

HaskellVipsImage *
haskell_vips_new_from_buffer (const void *original_buffer, guint32 length)
{
    HaskellVipsImage *haskell_image = NULL;
    VipsImage *image;
    const char *loader;
    void *buffer;

    /*
     * Vips assumes the passed-in buffer will be around for the VipsImage's
     * entire lifecycle.
    */
    buffer = g_memdup2 (original_buffer, length);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %p,%d", G_STRFUNC,
               original_buffer,buffer, length);
#endif

    if (!buffer)
        goto out;

#if HASKELL_VIPS_LOCKING
    g_mutex_lock (&lock_mutex);
#endif

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s start: %p,%d", G_STRFUNC, buffer, length);
#endif

    loader = vips_foreign_find_load_buffer (buffer, length);
#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s #1: %p,%d - %s", G_STRFUNC, buffer, length, loader);
#endif
    if (!loader) {
        g_free (buffer);
        goto out;
    }

    image = vips_image_new_from_buffer (buffer, (size_t)length, NULL, NULL);
#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s #2: %p,%d - %p", G_STRFUNC, buffer, length, (void*)image);
#endif
    if (!image) {
        g_free (buffer);
        goto out;
    }

    haskell_image = g_new0 (HaskellVipsImage, 1);
    haskell_image->image = image;
    haskell_image->type = image_type_from_loader (loader);
    haskell_image->input_buffer = buffer;
    haskell_image->input_buffer_size = length;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s done: %p - %p - %s", G_STRFUNC, (void*)haskell_image, (void*)image,
               get_image_type_suffix (haskell_image->type));
#endif

 out:
#if HASKELL_VIPS_LOCKING
    g_mutex_unlock (&lock_mutex);
#endif
    return haskell_image;
}

void
haskell_vips_free_image (HaskellVipsImage *image)
{
#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)image);
#endif

    VIPS_UNREF (image->image);
    if (image->input_buffer)
        g_free (image->input_buffer);
    g_free (image);
}

void
haskell_vips_get_image_size (const HaskellVipsImage *image, guint32 *width, guint32 *height)
{
    *width = vips_image_get_width (image->image);
    *height = vips_image_get_height (image->image);
}

int
haskell_vips_crop_image (HaskellVipsImage *image, guint32 left, guint32 top, guint32 width, guint32 height)
{
    VipsImage *out = NULL;
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p:%p", G_STRFUNC, (void*)image, (void*)image->image);
#endif

#if HASKELL_VIPS_LOCKING
    g_mutex_lock (&lock_mutex);
#endif

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s start: %p:%p", G_STRFUNC, (void*)image, (void*)image->image);
#endif

    ret = vips_crop (image->image, &out, left, top, width, height, NULL);
    if (ret) goto out;

    VIPS_UNREF (image->image);
    image->image = out;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s done: %p:%p", G_STRFUNC, (void*)image, (void*)image->image);
#endif

 out:
#if HASKELL_VIPS_LOCKING
    g_mutex_unlock (&lock_mutex);
#endif
    return 0;
}

int
haskell_vips_insert_image (HaskellVipsImage *main, HaskellVipsImage *overlay, guint32 left, guint32 top)
{
    VipsImage *out = NULL;
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p:%p - %p:%p", G_STRFUNC, (void*)main, (void*)main->image,
               (void*)overlay, (void*)overlay->image);
#endif

#if HASKELL_VIPS_LOCKING
    g_mutex_lock (&lock_mutex);
#endif

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s start: %p:%p - %p:%p", G_STRFUNC, (void*)main, (void*)main->image,
               (void*)overlay, (void*)overlay->image);
#endif

    ret = vips_insert (main->image, overlay->image, &out, left, top, NULL);
    if (ret) goto out;

    VIPS_UNREF (main->image);
    main->image = out;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s done: %p:%p - %p:%p", G_STRFUNC, (void*)main, (void*)main->image,
               (void*)overlay, (void*)overlay->image);
#endif

 out:
#if HASKELL_VIPS_LOCKING
    g_mutex_unlock (&lock_mutex);
#endif
    return 0;
}

int
haskell_vips_composite (HaskellVipsImage *main, HaskellVipsImage *overlay, VipsBlendMode blend, guint32 left, guint32 top)
{
    VipsImage *out = NULL;
    VipsImage *in[2];
    VipsArrayInt *x, *y;
    int xoffs[1], yoffs[1];
    int mode[1];
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p:%p - %p:%p", G_STRFUNC, (void*)main, (void*)main->image,
               (void*)overlay, (void*)overlay->image);
#endif

#if HASKELL_VIPS_LOCKING
    g_mutex_lock (&lock_mutex);
#endif

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s start: %p:%p - %p:%p", G_STRFUNC, (void*)main, (void*)main->image,
               (void*)overlay, (void*)overlay->image);
#endif

    in[0] = main->image;
    in[1] = overlay->image;
    mode[0] = blend;

    xoffs[0] = left;
    yoffs[0] = top;

    x = vips_array_int_new (xoffs, 1);
    y = vips_array_int_new (yoffs, 1);

    ret = vips_composite (in, &out, 2, mode, "x", x, "y", y, NULL);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s #1: %p:%p - %p - %d", G_STRFUNC, (void*)main, (void*)main->image, (void*)out, ret);
#endif

    vips_area_unref ((VipsArea *)x);
    vips_area_unref ((VipsArea *)y);

    if (ret) goto out;

    VIPS_UNREF (main->image);
    main->image = out;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s #2: %p:%p", G_STRFUNC, (void*)main, (void*)main->image);
#endif

    ret = vips_image_inplace (out);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s #3: %p:%p - %d", G_STRFUNC, (void*)main, (void*)main->image, ret);
#endif

    if (ret)
        goto out;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s done: %p:%p", G_STRFUNC, (void*)main, (void*)main->image);
#endif

out:
#if HASKELL_VIPS_LOCKING
    g_mutex_unlock (&lock_mutex);
#endif
    return ret;
}

gboolean
haskell_vips_image_has_alpha (HaskellVipsImage *image)
{
    return vips_image_hasalpha (image->image);
}

int
haskell_vips_image_get_bands (HaskellVipsImage *image)
{
    return vips_image_get_bands (image->image);
}

int
haskell_vips_add_alpha (HaskellVipsImage *image)
{
    VipsImage *out = NULL;
    int ret;

    ret = vips_addalpha (image->image, &out, NULL);
    if (ret) return ret;

    VIPS_UNREF (image->image);
    image->image = out;
    return 0;
}

void
haskell_vips_free_buffer (void *buffer)
{
    g_free (buffer);
}

int
haskell_vips_write_to_memory (HaskellVipsImage *image, void **retval, guint32 *size)
{
    size_t ssize = 0;
    const char *auto_suffix;
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p:%p", G_STRFUNC, (void*)image, (void*)image->image);
#endif

#if HASKELL_VIPS_LOCKING
    g_mutex_lock (&lock_mutex);
#endif

    auto_suffix = get_image_type_suffix (image->type);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s start: %p - %s", G_STRFUNC, (void*)image, auto_suffix);
#endif

    ret = vips_image_write_to_buffer (image->image, auto_suffix, retval, &ssize, NULL);
    *size = ssize;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s done: %p:%p - %d - %ld", G_STRFUNC, (void*)image, (void*)image->image, ret, ssize);
#endif

#if HASKELL_VIPS_LOCKING
    g_mutex_unlock (&lock_mutex);
#endif
    return ret;
}

void
haskell_vips_get_image_metadata (HaskellVipsImage *image, guint32 *width, guint32 *height, guint32 *density, guint32 *type)
{
    *width = vips_image_get_width (image->image);
    *height = vips_image_get_height (image->image);
    *density = vips_image_get_xres (image->image) * 25.4;
    *type = image->type;
}

void
haskell_vips_initialize (void)
{
    _haskell_vips_init ();
}

int
haskell_vips_resize (HaskellVipsImage *image, double scale)
{
    VipsImage *out = NULL;
    int ret;

    ret = vips_resize (image->image, &out, scale, NULL);
    if (ret) return ret;

    VIPS_UNREF (image->image);
    image->image = out;
    return 0;
}

int
haskell_vips_resize_copy (HaskellVipsImage *image, double scale, HaskellVipsImage **out)
{
    VipsImage *vout = NULL;
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)image, (void*)out);
#endif

    *out = NULL;
    ret = vips_resize (image->image, &vout, scale, NULL);
    if (ret) return ret;

    (*out) = g_new0 (HaskellVipsImage, 1);
    (*out)->image = vout;
    (*out)->type = image->type;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %p - %p", G_STRFUNC, (void*)image, (void*)out, (void*)(*out));
#endif

    return 0;
}

int
haskell_vips_pngsave (HaskellVipsImage *image, void **buffer, size_t *size)
{
    g_message (G_STRLOC ": %s: %p - %d - %p", G_STRFUNC, (void*)image,
               image->png_buffer_error, image->png_buffer);
    if (image->png_buffer_error)
        return image->png_buffer_error;

    if (image->png_buffer) {
        *buffer = image->png_buffer;
        *size = image->png_buffer_size;
        return 0;
    }

    image->png_buffer_error = vips_pngsave_buffer (image->image, &image->png_buffer,
                                                   &image->png_buffer_size, NULL);

    g_message (G_STRLOC ": %s #1: %p - %d - %p,%ld", G_STRFUNC, (void*)image,
               image->png_buffer_error, image->png_buffer, image->png_buffer_size);


    if (image->png_buffer_error < 0) {
        g_warning (G_STRLOC ": %s: failed to export image to PNG.", G_STRFUNC);
        return image->png_buffer_error;
    }

    *buffer = image->png_buffer;
    *size = image->png_buffer_size;

    return 0;
}

int
haskell_vips_webpsave (HaskellVipsImage *image, gint32 quality, void **buffer, size_t *size)
{
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)image);
#endif

    *buffer = NULL;

    ret = vips_webpsave_buffer (image->image, buffer, size, "Q", quality,
                                "min_size", FALSE, "effort", 6, "mixed", TRUE,
                                NULL);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %d - %p,%zu", G_STRFUNC, (void*)image,
               ret, *buffer, *size);
#endif

    return ret;
}

int
haskell_vips_avifsave (HaskellVipsImage *image, gint32 quality, void **buffer, size_t *size)
{
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)image);
#endif

    *buffer = NULL;

    ret = vips_heifsave_buffer (image->image, buffer, size, "Q", quality,
                                "compression", VIPS_FOREIGN_HEIF_COMPRESSION_AV1,
                                "subsample_mode", VIPS_FOREIGN_SUBSAMPLE_OFF,
                                "encoder", VIPS_FOREIGN_HEIF_ENCODER_AOM,
                                "bitdepth", 8, "effort", 6, NULL);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %d - %p,%zu", G_STRFUNC, (void*)image,
               ret, *buffer, *size);
#endif

    return ret;
}

int
haskell_vips_thumbnail (HaskellVipsImage *image, guint32 width, HaskellVipsImage **out)
{
    VipsImage *vout = NULL;
    int ret;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)image, (void*)out);
#endif

    *out = NULL;
    ret = vips_thumbnail_image (image->image, &vout, width, "size", VIPS_SIZE_DOWN, NULL);
    if (ret) return ret;

    (*out) = g_new0 (HaskellVipsImage, 1);
    (*out)->image = vout;
    (*out)->type = image->type;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %p - %p", G_STRFUNC, (void*)image, (void*)out, (void*)(*out));
#endif

    return 0;
}

HaskellVipsImage *
haskell_vips_thumbnail_buffer (const void *original_buffer, guint32 length, guint32 width)
{
    HaskellVipsImage *haskell_image = NULL;
    VipsImage *image;
    const char *loader;
    void *buffer;
    int ret;

    /*
     * Vips assumes the passed-in buffer will be around for the VipsImage's
     * entire lifecycle.
    */
    buffer = g_memdup2 (original_buffer, length);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s: %p - %p,%d", G_STRFUNC,
               original_buffer, buffer, length);
#endif

    if (!buffer)
        goto out;

#if HASKELL_VIPS_LOCKING
    g_mutex_lock (&lock_mutex);
#endif

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s start: %p,%d", G_STRFUNC, buffer, length);
#endif

    loader = vips_foreign_find_load_buffer (buffer, length);
#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s #1: %p,%d - %s", G_STRFUNC, buffer, length, loader);
#endif
    if (!loader) {
        g_free (buffer);
        goto out;
    }

    ret = vips_thumbnail_buffer ((void*)buffer, (size_t)length, &image, width,
                                 "size", VIPS_SIZE_DOWN, NULL);

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s #2: %p,%d - %d -> %p", G_STRFUNC,
               buffer, length, ret, (void*)image);
#endif
    if (ret) {
        g_free (buffer);
        goto out;
    }

    haskell_image = g_new0 (HaskellVipsImage, 1);
    haskell_image->image = image;
    haskell_image->type = image_type_from_loader (loader);
    haskell_image->input_buffer = buffer;
    haskell_image->input_buffer_size = length;

#if HASKELL_VIPS_DEBUG
    g_message (G_STRLOC ": %s done: %p - %p - %s", G_STRFUNC, (void*)haskell_image, (void*)image,
               get_image_type_suffix (haskell_image->type));
#endif

 out:
#if HASKELL_VIPS_LOCKING
    g_mutex_unlock (&lock_mutex);
#endif
    return haskell_image;
}
