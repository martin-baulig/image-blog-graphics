#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <vips/vips.h>

G_BEGIN_DECLS

typedef enum {
    IMAGE_TYPE_JPEG,
    IMAGE_TYPE_PNG,
    IMAGE_TYPE_WEBP,
    IMAGE_TYPE_TIFF,
    IMAGE_TYPE_GIF,
    IMAGE_TYPE_SVG,
    IMAGE_TYPE_HEIF,
    IMAGE_TYPE_PDF,
    IMAGE_TYPE_MAGICK,
    IMAGE_TYPE_OPENSLIDE,
    IMAGE_TYPE_PPM,
    IMAGE_TYPE_FITS,
    IMAGE_TYPE_VIPS,
    IMAGE_TYPE_RAW,
    IMAGE_TYPE_UNKNOWN,
    IMAGE_TYPE_MISSING
} ImageType;

typedef struct {
    VipsImage *image;
    ImageType type;
    void *png_buffer;
    size_t png_buffer_size;
    int png_buffer_error;
    void *input_buffer;
    size_t input_buffer_size;
} HaskellVipsImage;

void
haskell_vips_initialize (void);

HaskellVipsImage *
haskell_vips_open_image (const char *file);

HaskellVipsImage *
haskell_vips_new_from_buffer (const void *buffer, guint32 length);

void
haskell_vips_free_image (HaskellVipsImage *image);

void
haskell_vips_free_buffer (void *buffer);

const char *
haskell_vips_error_buffer (void);

void
haskell_vips_get_image_size (const HaskellVipsImage *image, guint32 *width, guint32 *height);

int
haskell_vips_crop_image (HaskellVipsImage *image, guint32 left, guint32 top, guint32 width, guint32 height);

int
haskell_vips_insert_image (HaskellVipsImage *main, HaskellVipsImage *overlay, guint32 left, guint32 top);

int
haskell_vips_composite (HaskellVipsImage *main, HaskellVipsImage *overlay, VipsBlendMode mode, guint32 left, guint32 top);

int
haskell_vips_write_to_memory (HaskellVipsImage *image, void **retval, guint32 *size);

void
haskell_vips_get_image_metadata (HaskellVipsImage *image, guint32 *width, guint32 *height, guint32 *density, guint32 *type);

gboolean
haskell_vips_image_has_alpha (HaskellVipsImage *image);

int
haskell_vips_image_get_bands (HaskellVipsImage *image);

int
haskell_vips_add_alpha (HaskellVipsImage *image);

int
haskell_vips_resize (HaskellVipsImage *image, double scale);

int
haskell_vips_resize_copy (HaskellVipsImage *image, double scale, HaskellVipsImage **out);

int
haskell_vips_pngsave (HaskellVipsImage *image, void **buffer, size_t *size);

int
haskell_vips_webpsave (HaskellVipsImage *image, gint32 quality, void **buffer, size_t *size);

int
haskell_vips_avifsave (HaskellVipsImage *image, gint32 quality, void **buffer, size_t *size);

int
haskell_vips_thumbnail (HaskellVipsImage *image, guint32 width, HaskellVipsImage **out);

HaskellVipsImage *
haskell_vips_thumbnail_buffer (const void *buffer, guint32 length, guint32 width);

G_END_DECLS
