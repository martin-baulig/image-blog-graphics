#include <ibg-buffer-source.h>
#include <ibg-utils.h>

#include <libavfilter/avfilter.h>

#include <ibg-ffmpeg-internal.h>

// -----------------------------------------------------------------------------
// -- IbgBufferSource
// -----------------------------------------------------------------------------

struct _IbgBufferSourcePrivate {
    char *name;
    AVFilterContext *filter;
};

static void
ibg_buffer_source_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgBufferSource, ibg_buffer_source, G_TYPE_OBJECT)

static void
ibg_buffer_source_class_init (IbgBufferSourceClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_buffer_source_finalize;
}

static void
ibg_buffer_source_init (IbgBufferSource *self)
{
    self->priv = ibg_buffer_source_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_buffer_source_finalize (GObject *object)
{
    IbgBufferSource *config = IBG_BUFFER_SOURCE (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)config);

    g_free (config->priv->name);

    (*G_OBJECT_CLASS (ibg_buffer_source_parent_class)->finalize) (object);
}

// -----------------------------------------------------------------------------
// -- BufferSourceContext
// -----------------------------------------------------------------------------

typedef struct BufferSourceContext {
    const AVClass *class;
    // config
    IbgFrameConfig *frame_config;
    int (* request_frame) (IbgFrame *frame);
    int32_t max_input_frames;
    // current state
    AVFrame *frame;
    int32_t num_buffered;
    int32_t num_frames;
    int64_t next_pts;
} BufferSourceContext;

static const AVOption haskell_buffer_source_options[] = {
    { NULL }
};

static const char *
default_item_name (void *ptr)
{
    return (*(AVClass **) ptr)->class_name;
}

static const AVClass haskell_buffer_source_class = {
    .class_name = "haskell-buffer-source",
    .item_name  = default_item_name,
    .option     = haskell_buffer_source_options,
    .version    = 1,
    .category   = AV_CLASS_CATEGORY_FILTER
};

static av_cold int
init_filter (AVFilterContext *ctx)
{
    BufferSourceContext *priv = ctx->priv;

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)ctx, (void*)priv);

    return 0;
}

static av_cold void
free_filter (AVFilterContext *ctx)
{
    BufferSourceContext *priv = ctx->priv;

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)ctx, (void*)priv);

    g_object_unref (priv->frame_config);
    av_frame_unref (priv->frame);
}

static int
config_props (AVFilterLink *link)
{
    BufferSourceContext *c = link->src->priv;

    g_message (G_STRLOC);

    switch (link->type) {
    case AVMEDIA_TYPE_VIDEO:
        link->w = c->frame_config->width;
        link->h = c->frame_config->height;
        link->sample_aspect_ratio = c->frame_config->aspect_ratio;

        break;

    default:
        return AVERROR(EINVAL);
    }

    link->time_base = c->frame_config->time_base;
    link->frame_rate = c->frame_config->frame_rate;

    return 0;
}

static int
request_frame (AVFilterLink *link)
{
    BufferSourceContext *c = link->src->priv;
    AVFrame *temp_frame;
    int ret;

    g_message (G_STRLOC ": %s: %p - %p - %ld", G_STRFUNC, (void*)link, (void*)c, c->next_pts);

    if (!c->frame) {
        g_error (G_STRLOC ": %s: must set config prior to use!", G_STRFUNC);
        return AVERROR_BUG;
    }

    if (c->max_input_frames && c->num_frames > c->max_input_frames)
        return AVERROR_EOF;

    c->frame->pts = c->next_pts++;

    if (!c->num_buffered) {
        IbgFrame *frame = ibg_frame_new_from_native (c->frame_config, c->frame, FALSE);

        if (!frame) {
            g_warning (G_STRLOC);
            return AVERROR (ENOMEM);
        }

        g_message (G_STRLOC ": %p - %ld - %ld", (void*)frame, c->frame->pts, c->next_pts);

        ret = c->request_frame (frame);

        g_message (G_STRLOC ": %d", ret);

        if (ret == 0)
            return AVERROR_EOF;
        if (ret < 0)
            return AVERROR_EXTERNAL;

        c->num_buffered += ret;

        g_object_unref (frame);
    }

    temp_frame = av_frame_clone (c->frame);
    if (!temp_frame) {
        g_warning (G_STRLOC);
        return AVERROR (ENOMEM);
    }

    g_message (G_STRLOC ": %p - %d - %ld", (void*)temp_frame, c->num_buffered, temp_frame->pts);

    c->num_buffered--;
    c->num_frames++;

    ret = ff_filter_frame (link, temp_frame);

    g_message (G_STRLOC ": %d - %ld", ret, temp_frame->pts);

    return ret;
}

static const AVFilterPad haskell_buffer_source_outputs[] = {
    {
        .name           = "default",
        .type           = AVMEDIA_TYPE_VIDEO,
        .request_frame  = request_frame,
        .config_props   = config_props
    },
    { NULL }
};

// -----------------------------------------------------------------------------
// -- Filter Definition
// -----------------------------------------------------------------------------

static const AVFilter haskell_buffer_source_filter = {
    .name               = "haskell-buffer-source",
    .description        = NULL,
    .priv_size          = sizeof (BufferSourceContext),
    .query_formats      = NULL,

    .init               = init_filter,
    .uninit             = free_filter,

    .inputs             = NULL,
    .outputs            = haskell_buffer_source_outputs,
    .priv_class         = &haskell_buffer_source_class
};

static int
buffer_source_set_config (AVFilterContext *ctx,
                          IbgFrameConfig *frame_config,
                          int32_t max_input_frames,
                          int (*request_frame) (IbgFrame *))
{
    BufferSourceContext *priv = ctx->priv;

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)ctx, (void*)priv);

    if (priv->frame) {
        g_error (G_STRLOC ": %s: duplicate init!", G_STRFUNC);
        return AVERROR_BUG;
    }

    priv->frame_config = g_object_ref (frame_config);
    priv->max_input_frames = max_input_frames;
    priv->request_frame = request_frame;

    priv->frame = ibg_frame_alloc_raw_frame (frame_config->pix_fmt, frame_config->width, frame_config->height);
    if (!priv->frame) {
        g_warning (G_STRLOC ": %s: failed to init frame.", G_STRFUNC);
        return AVERROR (ENOMEM);
    }

    return 0;
}

IbgBufferSource *
ibg_buffer_source_new (IbgFilterGraph *graph,
                       int32_t input_index,
                       int32_t max_input_frames,
                       int (*request_frame) (IbgFrame *))
{
    IbgBufferSource *source;
    AVFilterContext *filter;
    char name[512];
    int ret;

    g_message (G_STRLOC ": %s: %p - %d - %d", G_STRFUNC, (void*)graph, input_index, max_input_frames);

    if (input_index > 1)
        snprintf (name, 512, "in%d", input_index+1);
    else
        strncpy (name, "in", 512);

    source = g_object_new (IBG_TYPE_BUFFER_SOURCE, NULL);
    if (!source) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    g_message (G_STRLOC ": %p - %s", (void*)graph, name);

    ret = avfilter_graph_create_filter (&filter, &haskell_buffer_source_filter, "in", NULL, NULL,
                                        ibg_filter_graph_get_graph (graph));
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        g_object_unref (source);
        return NULL;
    }

    g_message (G_STRLOC ": %p", (void*)filter);

    ret = buffer_source_set_config (filter, ibg_filter_graph_get_config (graph),
                                    max_input_frames, request_frame);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        g_object_unref (source);
        return NULL;
    }

    source->priv->name = g_strdup (name);
    if (!source->priv->name) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        g_object_unref (source);
        return NULL;
    }

    source->priv->filter = filter;

    return source;
}

AVFilterInOut *
ibg_buffer_source_get_output (IbgBufferSource *source)
{
    AVFilterInOut *inputs;

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)source);

    inputs = avfilter_inout_alloc ();

    if (!inputs) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    inputs->name       = av_strdup (source->priv->name);
    inputs->filter_ctx = source->priv->filter;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;

    if (!inputs->name) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        avfilter_inout_free (&inputs);
        return NULL;
    }

    return inputs;
}
