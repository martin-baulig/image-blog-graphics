#ifndef __IBG_BUFFER_SOURCE_H__
#define __IBG_BUFFER_SOURCE_H__ 1

#include <ibg-frame.h>
#include <ibg-filter-graph.h>

G_BEGIN_DECLS

typedef struct _IbgBufferSource                 IbgBufferSource;
typedef struct _IbgBufferSourceClass            IbgBufferSourceClass;
typedef struct _IbgBufferSourcePrivate          IbgBufferSourcePrivate;

#define IBG_TYPE_BUFFER_SOURCE                  (ibg_buffer_source_get_type ())
#define IBG_BUFFER_SOURCE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_BUFFER_SOURCE, IbgBufferSource))
#define IBG_IS_BUFFER_SOURCE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_BUFFER_SOURCE))
#define IBG_BUFFER_SOURCE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_BUFFER_SOURCE, IbgBufferSourceClass))
#define IBG_IS_BUFFER_SOURCE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_BUFFER_SOURCE))
#define IBG_BUFFER_SOURCE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_BUFFER_SOURCE, IbgBufferSourceClass))

struct _IbgBufferSource
{
    GObject parent_instance;

    IbgBufferSourcePrivate *priv;
};

struct _IbgBufferSourceClass
{
    GObjectClass parent_class;
};

GType
ibg_buffer_source_get_type (void);

IbgBufferSource *
ibg_buffer_source_new (IbgFilterGraph *graph,
                       int32_t input_index,
                       int32_t max_input_frames,
                       int (*request_frame) (IbgFrame *));

AVFilterInOut *
ibg_buffer_source_get_output (IbgBufferSource *source);

G_END_DECLS

#endif
