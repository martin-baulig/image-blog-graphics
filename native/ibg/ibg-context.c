#include <ibg-context.h>
#include <ibg-stream.h>
#include <ibg-utils.h>

#include <libavutil/log.h>
#include <libavutil/timestamp.h>

#if DEBUG_RELOC
#include <ctype.h>
#endif

#define RNDTO2(X)               ((X) & 0xFFFFFFFE)
#define RNDTO32(X)              (((X) % 32 ) ? (((X) + 32) & 0xFFFFFFE0) : (X))


struct _IbgContextPrivate {
    AVFormatContext *context;
};

static void
ibg_context_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgContext, ibg_context, G_TYPE_OBJECT)

static void
ibg_context_class_init (IbgContextClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_context_finalize;
}

static void
ibg_context_init (IbgContext *self)
{
    self->priv = ibg_context_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_context_finalize (GObject *object)
{
    IbgContext *context = IBG_CONTEXT (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)context);

    if (context->priv->context)
        avformat_free_context (context->priv->context);

    (*G_OBJECT_CLASS (ibg_context_parent_class)->finalize) (object);
}

IbgContext *
ibg_context_new (void)
{
    AVFormatContext *ctx = avformat_alloc_context ();
    IbgContext *context;

    context = g_object_new (IBG_TYPE_CONTEXT, NULL);
    if (!context) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        avformat_free_context (ctx);
        return NULL;
    }

    if (!ctx)
        return NULL;

    context->priv->context = ctx;
    return context;
}

IbgContext *
ibg_context_new_output (void)
{
    IbgContext *context;
    int ret;

    context = g_object_new (IBG_TYPE_CONTEXT, NULL);
    if (!context)
        return NULL;

    ret = avformat_alloc_output_context2 (&context->priv->context, NULL, OUTPUT_FORMAT, NULL);
    g_message (G_STRLOC ": haskell_ffmpeg_alloc_output_context: %d - %p", ret, (void *)context->priv->context);

    if (!context->priv->context) {
        ibg_print_error (G_STRLOC, ret);
        g_object_unref (context);
        return NULL;
    }

    return context;
}

AVFormatContext *
ibg_context_get_context (const IbgContext *context)
{
    return context->priv->context;
}

static void *
init_once (void *ptr) {
    avdevice_register_all ();
    av_log_set_level (AV_LOG_DEBUG);
    return NULL;
}

void
ibg_initialize (void)
{
    static GOnce ibg_init_once = G_ONCE_INIT;
    g_once (&ibg_init_once, init_once, NULL);
}

int
ibg_context_begin_write (IbgContext *context, IbgIoOutput *io)
{
    int ret;

    g_message (G_STRLOC ": %s", G_STRFUNC);

    context->priv->context->pb = ibg_io_get_context (IBG_IO (io));

    av_dump_format (context->priv->context, 0, NULL, 1);

    ret = avformat_write_header (context->priv->context, NULL);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    return 0;
}

int
ibg_context_finish_write (IbgContext *context)
{
    int ret;

    g_message (G_STRLOC ": %s", G_STRFUNC);

    ret = av_write_trailer (context->priv->context);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    return 0;
}
