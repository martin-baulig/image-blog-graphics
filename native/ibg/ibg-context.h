#ifndef __IBG_CONTEXT_H__
#define __IBG_CONTEXT_H__ 1

#include <ibg-decls.h>
#include <ibg-io.h>

G_BEGIN_DECLS

typedef struct _IbgContext                 IbgContext;
typedef struct _IbgContextClass            IbgContextClass;
typedef struct _IbgContextPrivate          IbgContextPrivate;

#define IBG_TYPE_CONTEXT                  (ibg_context_get_type ())
#define IBG_CONTEXT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_CONTEXT, IbgContext))
#define IBG_IS_CONTEXT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_CONTEXT))
#define IBG_CONTEXT_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_CONTEXT, IbgContextClass))
#define IBG_IS_CONTEXT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_CONTEXT))
#define IBG_CONTEXT_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_CONTEXT, IbgContextClass))

struct _IbgContext
{
    GObject parent_instance;

    IbgContextPrivate *priv;
};

struct _IbgContextClass
{
    GObjectClass parent_class;
};

GType
ibg_context_get_type (void);

IbgContext *
ibg_context_new (void);

IbgContext *
ibg_context_new_output (void);

AVFormatContext *
ibg_context_get_context (const IbgContext *context);

void
ibg_initialize (void);

int
ibg_context_begin_write (IbgContext *context, IbgIoOutput *io);

int
ibg_context_finish_write (IbgContext *context);

G_END_DECLS

#endif
