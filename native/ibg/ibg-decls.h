#ifndef __IBG_DECLS_H__
#define __IBG_DECLS_H__ 1

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <glib.h>
#include <glib-object.h>

#include <libavutil/imgutils.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavdevice/avdevice.h>

G_BEGIN_DECLS

typedef struct _IbgIoOutput                 IbgIoOutput;

G_END_DECLS

#endif
