#include <ibg-filter-graph.h>

struct _IbgFilterGraphPrivate {
    IbgFrameConfig *config;
    AVFilterGraph *graph;
};

static void
ibg_filter_graph_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgFilterGraph, ibg_filter_graph, G_TYPE_OBJECT)

static void
ibg_filter_graph_class_init (IbgFilterGraphClass *klass)
{
    GObjectClass   *gobject_class;

    g_message (G_STRLOC);

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_filter_graph_finalize;
}

static void
ibg_filter_graph_init (IbgFilterGraph *self)
{
    g_message (G_STRLOC);

    self->priv = ibg_filter_graph_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_filter_graph_finalize (GObject *object)
{
    IbgFilterGraph *graph = IBG_FILTER_GRAPH (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)graph);

    if (graph->priv->graph)
        avfilter_graph_free (&graph->priv->graph);
    if (graph->priv->config)
        g_object_unref (graph->priv->config);

    (*G_OBJECT_CLASS (ibg_filter_graph_parent_class)->finalize) (object);
}

IbgFilterGraph *
ibg_filter_graph_new (IbgFrameConfig *config)
{
    IbgFilterGraph *graph;

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)config);

    graph = g_object_new (IBG_TYPE_FILTER_GRAPH, NULL);
    if (!graph)
        return NULL;

    graph->priv->graph = avfilter_graph_alloc ();
    if (!graph->priv->graph) {
        g_object_unref (graph);
        return NULL;
    }

    graph->priv->config = g_object_ref (config);

    g_message (G_STRLOC ": %p", (void*)graph);

    return graph;
}

AVFilterGraph *
ibg_filter_graph_get_graph (IbgFilterGraph *graph)
{
    return graph->priv->graph;
}

IbgFrameConfig *
ibg_filter_graph_get_config (IbgFilterGraph *graph)
{
    return graph->priv->config;
}
