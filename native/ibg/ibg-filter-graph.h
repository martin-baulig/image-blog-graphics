#ifndef __IBG_FILTER_GRAPH_H__
#define __IBG_FILTER_GRAPH_H__ 1

#include <ibg-frame-config.h>

#include <libavfilter/avfilter.h>

G_BEGIN_DECLS

typedef struct _IbgFilterGraph                 IbgFilterGraph;
typedef struct _IbgFilterGraphClass            IbgFilterGraphClass;
typedef struct _IbgFilterGraphPrivate          IbgFilterGraphPrivate;

#define IBG_TYPE_FILTER_GRAPH                  (ibg_filter_graph_get_type ())
#define IBG_FILTER_GRAPH(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_FILTER_GRAPH, IbgFilterGraph))
#define IBG_IS_FILTER_GRAPH(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_FILTER_GRAPH))
#define IBG_FILTER_GRAPH_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_FILTER_GRAPH, IbgFilterGraphClass))
#define IBG_IS_FILTER_GRAPH_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_FILTER_GRAPH))
#define IBG_FILTER_GRAPH_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_FILTER_GRAPH, IbgFilterGraphClass))

struct _IbgFilterGraph
{
    GObject parent_instance;

    IbgFilterGraphPrivate *priv;
};

struct _IbgFilterGraphClass
{
    GObjectClass parent_class;
};

GType
ibg_filter_graph_get_type (void);

IbgFilterGraph *
ibg_filter_graph_new (IbgFrameConfig *config);

AVFilterGraph *
ibg_filter_graph_get_graph (IbgFilterGraph *graph);

IbgFrameConfig *
ibg_filter_graph_get_config (IbgFilterGraph *graph);

G_END_DECLS

#endif
