#include <ibg-filter.h>
#include <ibg-utils.h>

#include <libavfilter/buffersink.h>

struct _IbgFilterPrivate {
    IbgFilterGraph *graph;
    AVFilterContext *first_input;
    AVFilterContext *second_input;
    AVFilterContext *output;
    int64_t next_pts;
};

static void
ibg_filter_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgFilter, ibg_filter, G_TYPE_OBJECT)

static void
ibg_filter_class_init (IbgFilterClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_filter_finalize;
}

static void
ibg_filter_init (IbgFilter *self)
{
    self->priv = ibg_filter_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_filter_finalize (GObject *object)
{
    IbgFilter *filter = IBG_FILTER (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)filter);

    (*G_OBJECT_CLASS (ibg_filter_parent_class)->finalize) (object);
}

static AVFilterInOut *
create_outputs (IbgFrameConfig *config, AVFilterGraph *graph)
{
    const AVFilter *buffersink;
    enum AVPixelFormat pix_fmt;
    AVFilterContext *filter;
    AVFilterInOut *outputs;
    char args[512];
    int ret;

    outputs = avfilter_inout_alloc ();

    if (!outputs) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        goto error;
    }

    buffersink = avfilter_get_by_name ("buffersink");
    if (!buffersink) {
        ibg_print_error (G_STRLOC, AVERROR_FILTER_NOT_FOUND);
        goto error;
    }

    ibg_frame_config_print (config, args, sizeof (args));

    g_message (G_STRLOC ": %p - %p - %ps", (void*)graph, (void*)outputs, (void*)buffersink);

    ret = avfilter_graph_create_filter (&filter, buffersink, "out", NULL, NULL, graph);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        goto error;
    }

    pix_fmt = config->pix_fmt;

    ret = av_opt_set_bin (filter, "pix_fmts",
                          (uint8_t*)&pix_fmt, sizeof(pix_fmt),
                          AV_OPT_SEARCH_CHILDREN);

    outputs->name        = av_strdup ("out");
    outputs->filter_ctx  = filter;
    outputs->pad_idx     = 0;
    outputs->next        = NULL;

    if (!outputs->name) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        goto error;
    }

    return outputs;

error:
    avfilter_inout_free (&outputs);
    return NULL;
}

IbgFilter *
ibg_filter_new (IbgFilterGraph *graph, const char *filter_spec,
                IbgBufferSource *buffer_source,
                IbgBufferSource *second_source)
{
    IbgFilter *filter;
    AVFilterGraph *raw_graph;
    IbgFrameConfig *frame_config;
    AVFilterInOut *outputs;
    AVFilterInOut *inputs;
    int ret;

    g_message (G_STRLOC ": %s - %s - %p - %p", G_STRFUNC, filter_spec,
               (void*)buffer_source, (void*)second_source);

    filter = g_object_new (IBG_TYPE_FILTER, NULL);
    if (!filter) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    filter->priv->graph = g_object_ref (graph);

    raw_graph = ibg_filter_graph_get_graph (filter->priv->graph);
    frame_config = ibg_filter_graph_get_config (filter->priv->graph);

    inputs = ibg_buffer_source_get_output (buffer_source);
    outputs = create_outputs (frame_config, raw_graph);

    if (!inputs || !outputs) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        goto error;
    }

    g_message (G_STRLOC ": %p - %p - %p - %s", (void*)inputs, (void*)inputs->next, (void*)outputs, filter_spec);

    filter->priv->first_input = inputs->filter_ctx;
    filter->priv->output = outputs->filter_ctx;

    ret = avfilter_graph_parse_ptr (raw_graph, filter_spec, &outputs, &inputs, NULL);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        goto error;
    }

    g_message (G_STRLOC ": %p - %p", (void*)outputs, (void*)inputs);

    while (outputs) {
        g_message (G_STRLOC ": out %p: %s - %p - %s - %d - %p", (void*)outputs, outputs->name, (void*)outputs->filter_ctx,
                   outputs->filter_ctx->filter->name, outputs->pad_idx, (void*)outputs->next);
        outputs = outputs->next;
    }

    while (inputs) {
        g_message (G_STRLOC ": in %p: %s - %p - %s - %d - %p", (void*)inputs, inputs->name, (void*)inputs->filter_ctx,
                   inputs->filter_ctx->filter->name, inputs->pad_idx, (void*)inputs->next);
        inputs = inputs->next;
    }

    ret = avfilter_graph_config (raw_graph, NULL);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        goto error;
    }

    g_message (G_STRLOC ": %s", avfilter_graph_dump (raw_graph, NULL));

    avfilter_inout_free (&inputs);
    avfilter_inout_free (&outputs);

    return filter;

error:
    g_object_unref (filter);
    avfilter_inout_free (&inputs);
    avfilter_inout_free (&outputs);
    return NULL;
}

int
ibg_filter_read_frame (IbgFilter *filter, IbgFrame **frame)
{
    IbgFrameConfig *config = ibg_filter_graph_get_config (filter->priv->graph);
    AVFrame *raw_frame = av_frame_alloc ();
    int ret;

    *frame = NULL;

    if (!raw_frame) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return -1;
    }

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)frame, (void*)raw_frame);

    ret = av_buffersink_get_frame (filter->priv->output, raw_frame);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
        g_message (G_STRLOC ": %d - %s", ret, ret == AVERROR(EAGAIN) ? "again" : "eof");
        av_frame_unref (raw_frame);
        return 1;
    }

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        av_frame_unref (raw_frame);
        return ret;
    }

    *frame = ibg_frame_new_from_native (config, raw_frame, TRUE);

    if (!*frame) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        av_frame_unref (raw_frame);
        return -1;
    }

    return 0;
}

IbgFrameConfig *
ibg_filter_get_frame_config (IbgFilter *filter)
{
    return ibg_filter_graph_get_config (filter->priv->graph);
}
