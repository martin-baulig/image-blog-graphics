#ifndef __IBG_FILTER_H__
#define __IBG_FILTER_H__ 1

#include <ibg-frame.h>
#include <ibg-filter-graph.h>
#include <ibg-buffer-source.h>

G_BEGIN_DECLS

typedef struct _IbgFilter                 IbgFilter;
typedef struct _IbgFilterClass            IbgFilterClass;
typedef struct _IbgFilterPrivate          IbgFilterPrivate;

#define IBG_TYPE_FILTER                  (ibg_filter_get_type ())
#define IBG_FILTER(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_FILTER, IbgFilter))
#define IBG_IS_FILTER(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_FILTER))
#define IBG_FILTER_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_FILTER, IbgFilterClass))
#define IBG_IS_FILTER_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_FILTER))
#define IBG_FILTER_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_FILTER, IbgFilterClass))

struct _IbgFilter
{
    GObject parent_instance;

    IbgFilterPrivate *priv;
};

struct _IbgFilterClass
{
    GObjectClass parent_class;
};

GType
ibg_filter_get_type (void);

IbgFilter *
ibg_filter_new (IbgFilterGraph *graph, const char *filter_spec,
                IbgBufferSource *buffer_source,
                IbgBufferSource *second_source);

IbgFrameConfig *
ibg_filter_get_frame_config (IbgFilter *filter);

int
ibg_filter_read_frame (IbgFilter *filter, IbgFrame **frame);

G_END_DECLS

#endif
