#include <ibg-frame-config.h>
#include <ibg-stream.h>

static void
ibg_frame_config_finalize (GObject *object);

static volatile gint alloc_count = 0;

G_DEFINE_TYPE (IbgFrameConfig, ibg_frame_config, G_TYPE_OBJECT)

static void
ibg_frame_config_class_init (IbgFrameConfigClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_frame_config_finalize;
}

static void
ibg_frame_config_init (IbgFrameConfig *self)
{
    gint allocs = g_atomic_int_add (&alloc_count, 1);

    g_message (G_STRLOC ": %s: %p - %d", G_STRFUNC, (void*)self, allocs);
}

static void
ibg_frame_config_finalize (GObject *object)
{
    IbgFrameConfig *config = IBG_FRAME_CONFIG (object);
    gint allocs = g_atomic_int_add (&alloc_count, -1);

    g_message (G_STRLOC ": %s: %p - %d", G_STRFUNC, (void*)config, allocs);

    (*G_OBJECT_CLASS (ibg_frame_config_parent_class)->finalize) (object);
}

IbgFrameConfig *
ibg_frame_config_new (enum AVPixelFormat pix_fmt, int width, int height,
                      AVRational time_base, AVRational aspect_ratio)
{
    IbgFrameConfig *config;

    config = g_object_new (IBG_TYPE_FRAME_CONFIG, NULL);
    if (!config)
        return NULL;

    config->pix_fmt = pix_fmt;
    config->width = width;
    config->height = height;
    config->time_base = time_base;
    config->aspect_ratio = aspect_ratio;
    config->frame_rate = av_make_q (1, 25);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)config);

    return config;
}

IbgFrameConfig *
ibg_frame_config_new2 (IbgFrameConfigData *data)
{
    return ibg_frame_config_new (data->pix_fmt, data->width, data->height,
                                 av_make_q (data->time_base.num, data->time_base.den),
                                 av_make_q (data->aspect_ratio.num, data->aspect_ratio.den));
}

static AVRational
compute_aspect_ratio (guint32 width, guint32 height)
{
    AVRational aspect_ratio;

    av_reduce (&aspect_ratio.num, &aspect_ratio.den, width, height, INT_MAX);

    if (aspect_ratio.num <= 0 || aspect_ratio.den <= 0) {
        g_warning (G_STRLOC ": %s - failed to compute aspect ratio for %d / %d.", G_STRFUNC, width, height);
        return av_make_q (width, height);
    }

    return aspect_ratio;
}

IbgFrameConfig *
ibg_frame_config_new_from_resolution (IbgVideoResolution resolution)
{
    guint32 width, height;
    AVRational aspect_ratio;

    if (ibg_get_resolution (resolution, &width, &height) < 0) {
        g_warning (G_STRLOC ": %s: invalid video resolution: %d", G_STRFUNC, resolution);
        return NULL;
    }

    aspect_ratio = compute_aspect_ratio (width, height);

    return ibg_frame_config_new (STREAM_PIX_FMT, width, height, av_make_q (1, STREAM_FRAME_RATE), aspect_ratio);
}

void
ibg_frame_config_get_data (IbgFrameConfig *config, IbgFrameConfigData *data)
{
    data->pix_fmt = config->pix_fmt;
    data->width = config->width;
    data->height = config->height;
    data->time_base.num = config->time_base.num;
    data->time_base.den = config->time_base.den;
    data->aspect_ratio.num = config->aspect_ratio.num;
    data->aspect_ratio.den = config->aspect_ratio.den;
    data->frame_rate.num = config->frame_rate.num;
    data->frame_rate.den = config->frame_rate.den;
}

void
ibg_frame_config_print (IbgFrameConfig *config, char *buffer, size_t size)
{
    snprintf (buffer, size,
              "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
              config->width, config->height, config->pix_fmt,
              config->time_base.num, config->time_base.den,
              config->aspect_ratio.num,
              config->aspect_ratio.den);
}

IbgFrameConfig *
ibg_frame_config_dup (IbgFrameConfig *config)
{
    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)config);
    return g_object_ref (config);
}
