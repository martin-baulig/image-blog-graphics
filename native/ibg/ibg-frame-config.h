#ifndef __IBG_FRAME_CONFIG_H__
#define __IBG_FRAME_CONFIG_H__ 1

#include <ibg-decls.h>
#include <ibg-resolution.h>

G_BEGIN_DECLS

typedef struct _IbgFrameConfig                 IbgFrameConfig;
typedef struct _IbgFrameConfigClass            IbgFrameConfigClass;

#define IBG_TYPE_FRAME_CONFIG                  (ibg_frame_config_get_type ())
#define IBG_FRAME_CONFIG(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_FRAME_CONFIG, IbgFrameConfig))
#define IBG_IS_FRAME_CONFIG(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_FRAME_CONFIG))
#define IBG_FRAME_CONFIG_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_FRAME_CONFIG, IbgFrameConfigClass))
#define IBG_IS_FRAME_CONFIG_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_FRAME_CONFIG))
#define IBG_FRAME_CONFIG_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_FRAME_CONFIG, IbgFrameConfigClass))

struct _IbgFrameConfig
{
    GObject parent_instance;

    enum AVPixelFormat pix_fmt;
    int width;
    int height;
    AVRational time_base;
    AVRational aspect_ratio;
    AVRational frame_rate;
};

struct _IbgFrameConfigClass
{
    GObjectClass parent_class;
};

typedef struct {
    guint32 num, den;
} IbgRational;

typedef struct {
    guint32 pix_fmt;
    guint32 width;
    guint32 height;
    guint32 reserved;
    IbgRational time_base;
    IbgRational aspect_ratio;
    IbgRational frame_rate;
} IbgFrameConfigData;


GType
ibg_frame_config_get_type (void);

IbgFrameConfig *
ibg_frame_config_new (enum AVPixelFormat pix_fmt, int width, int height,
                      AVRational time_base, AVRational aspect_ratio);

IbgFrameConfig *
ibg_frame_config_new_from_resolution (IbgVideoResolution resolution);

IbgFrameConfig *
ibg_frame_config_new2 (IbgFrameConfigData *config_data);

void
ibg_frame_config_get_data (IbgFrameConfig *config, IbgFrameConfigData *data);

void
ibg_frame_config_print (IbgFrameConfig *config, char *buffer, size_t size);

IbgFrameConfig *
ibg_frame_config_dup (IbgFrameConfig *config);

G_END_DECLS

#endif
