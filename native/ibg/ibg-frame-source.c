#include <ibg-frame-source.h>
#include <ibg-utils.h>

struct _IbgFrameSourcePrivate {
    IbgFrameConfig *config;
    AVFrame *frame;
};

static void
ibg_frame_source_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgFrameSource, ibg_frame_source, G_TYPE_OBJECT)

static void
ibg_frame_source_class_init (IbgFrameSourceClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_frame_source_finalize;
}

static void
ibg_frame_source_init (IbgFrameSource *self)
{
    self->priv = ibg_frame_source_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_frame_source_finalize (GObject *object)
{
    IbgFrameSource *self = IBG_FRAME_SOURCE (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)self);

    (*G_OBJECT_CLASS (ibg_frame_source_parent_class)->finalize) (object);
}

IbgFrameSource *
ibg_frame_source_new (IbgFrameConfig *config)
{
    IbgFrameSource *source;

    source = g_object_new (IBG_TYPE_FRAME_SOURCE, NULL);
    if (!source) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    source->priv->config = g_object_ref (config);

    source->priv->frame = ibg_frame_alloc_raw_frame (config->pix_fmt, config->width, config->height);

    if (!source->priv->frame) {
        g_warning (G_STRLOC ": %s: failed to alloc frame.", G_STRFUNC);
        g_object_unref (source);
        return NULL;
    }

    return source;
}

IbgFrameConfig *
ibg_frame_source_get_config (IbgFrameSource *source)
{
    return g_object_ref (source->priv->config);
}
