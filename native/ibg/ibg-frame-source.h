#ifndef __IBG_FRAME_SOURCE_H__
#define __IBG_FRAME_SOURCE_H__ 1

#include <ibg-frame.h>

G_BEGIN_DECLS

typedef struct _IbgFrameSource                 IbgFrameSource;
typedef struct _IbgFrameSourceClass            IbgFrameSourceClass;
typedef struct _IbgFrameSourcePrivate          IbgFrameSourcePrivate;

#define IBG_TYPE_FRAME_SOURCE                  (ibg_frame_source_get_type ())
#define IBG_FRAME_SOURCE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_FRAME_SOURCE, IbgFrameSource))
#define IBG_IS_FRAME_SOURCE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_FRAME_SOURCE))
#define IBG_FRAME_SOURCE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_FRAME_SOURCE, IbgFrameSourceClass))
#define IBG_IS_FRAME_SOURCE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_FRAME_SOURCE))
#define IBG_FRAME_SOURCE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_FRAME_SOURCE, IbgFrameSourceClass))

struct _IbgFrameSource
{
    GObject parent_instance;

    IbgFrameSourcePrivate *priv;
};

struct _IbgFrameSourceClass
{
    GObjectClass parent_class;
};

IbgFrameSource *
ibg_frame_source_new (IbgFrameConfig *config);

IbgFrameConfig *
ibg_frame_source_get_config (IbgFrameSource *source);

G_END_DECLS

#endif
