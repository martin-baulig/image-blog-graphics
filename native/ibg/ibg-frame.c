#include <ibg-frame.h>
#include <ibg-stream.h>
#include <ibg-utils.h>

#include <libavutil/log.h>
#include <libavutil/timestamp.h>

#include <libswscale/swscale.h>

struct _IbgFramePrivate {
    IbgFrameConfig *config;
    AVFrame *frame;
};

static void
ibg_frame_finalize (GObject *object);

static volatile gint alloc_count = 0;

G_DEFINE_TYPE_WITH_PRIVATE (IbgFrame, ibg_frame, G_TYPE_OBJECT)

static void
ibg_frame_class_init (IbgFrameClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_frame_finalize;
}

static void
ibg_frame_init (IbgFrame *self)
{
    gint allocs;

    self->priv = ibg_frame_get_instance_private (self);

    allocs = g_atomic_int_add (&alloc_count, 1);

    g_message (G_STRLOC ": %s: %p - %p - %d", G_STRFUNC, (void*)self, (void*)self->priv, allocs);
}

static void
ibg_frame_finalize (GObject *object)
{
    IbgFrame *frame = IBG_FRAME (object);
    gint allocs = g_atomic_int_add (&alloc_count, -1);

    g_message (G_STRLOC ": %s: %p - %p - %d", G_STRFUNC, (void*)frame, (void*)frame->priv->frame, allocs-1);

    if (frame->priv->config)
        g_object_unref (frame->priv->config);
    if (frame->priv->frame)
        av_frame_free (&frame->priv->frame);

    (*G_OBJECT_CLASS (ibg_frame_parent_class)->finalize) (object);
}

AVFrame *
ibg_frame_alloc_raw_frame (enum AVPixelFormat pix_fmt, int width, int height)
{
    AVFrame *picture;
    int ret;

    picture = av_frame_alloc ();
    if (!picture)
        return NULL;

    picture->format = pix_fmt;
    picture->width  = width;
    picture->height = height;

    picture->pts = 0;

    ret = av_frame_get_buffer (picture, 0);
    if (ret < 0)
        return NULL;

    return picture;
}

IbgFrame *
ibg_frame_new (IbgFrameConfig *config)
{
    IbgFrame *frame;

    frame = g_object_new (IBG_TYPE_FRAME, NULL);
    if (!frame) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    frame->priv->config = g_object_ref (config);

    frame->priv->frame = ibg_frame_alloc_raw_frame (config->pix_fmt, config->width, config->height);

    if (!frame->priv->frame) {
        g_warning (G_STRLOC ": %s: failed to alloc frame.", G_STRFUNC);
        g_object_unref (frame);
        return NULL;
    }

    return frame;
}

IbgFrame *
ibg_frame_new_from_native (IbgFrameConfig *config, AVFrame *source, gboolean owns)
{
    IbgFrame *frame;

    frame = g_object_new (IBG_TYPE_FRAME, NULL);
    if (!frame) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    frame->priv->config = g_object_ref (config);

    frame->priv->frame = owns ? source : av_frame_clone (source);

    if (!frame->priv->frame) {
        g_warning (G_STRLOC ": %s: failed to alloc frame.", G_STRFUNC);
        g_object_unref (frame);
        return NULL;
    }

    return frame;
}

IbgFrameConfig *
ibg_frame_get_config (IbgFrame *frame)
{
    return g_object_ref (frame->priv->config);
}

int
ibg_frame_copy_raw_frame (IbgFrame *frame, const AVFrame *source)
{
    return av_frame_copy (frame->priv->frame, source);
}

int
ibg_frame_copy_frame (IbgFrame *frame, const IbgFrame *source)
{
    return av_frame_copy (frame->priv->frame, source->priv->frame);
}

AVFrame *
ibg_frame_get_frame (const IbgFrame *frame)
{
    return frame->priv->frame;
}

void
ibg_frame_get_dimensions (const IbgFrame *frame, guint32 *width, guint32 *height)
{
    *width = frame->priv->frame->width;
    *height = frame->priv->frame->height;
}

AVFrame *
ibg_frame_scale_raw_frame (AVFrame *input_frame, guint32 width, guint32 height)
{
    struct SwsContext *sws;
    AVFrame *frame;
    int ret;

    g_message (G_STRLOC ": %s: %p - %p - %p - %d -> %d,%d", G_STRFUNC,
               (void*)input_frame, (void*)input_frame->data, (void*)input_frame->linesize, input_frame->format,
               width, height);

    sws = sws_getContext (input_frame->width, input_frame->height, input_frame->format,
                          width, height, STREAM_PIX_FMT,
                          SWS_LANCZOS | SWS_ACCURATE_RND, NULL, NULL, NULL);

    if (!sws) {
        g_warning (G_STRLOC ": %s: failed to allocate sws context.", G_STRFUNC);
        return NULL;
    }

    frame = ibg_frame_alloc_raw_frame (STREAM_PIX_FMT, width, height);

    g_message (G_STRLOC ": %s: %p - %p - %p", G_STRFUNC, (void*)frame, (void*)frame->data, (void*)frame->linesize);

    ret = sws_scale (sws, (const uint8_t * const *)input_frame->data, input_frame->linesize,
                     0, input_frame->height, frame->data, frame->linesize);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        sws_freeContext (sws);
        return NULL;
    }

    sws_freeContext (sws);
    return frame;
}
