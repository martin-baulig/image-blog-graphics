#ifndef __IBG_FRAME_H__
#define __IBG_FRAME_H__ 1

#include <ibg-frame-config.h>

G_BEGIN_DECLS

typedef struct _IbgFrame                 IbgFrame;
typedef struct _IbgFrameClass            IbgFrameClass;
typedef struct _IbgFramePrivate          IbgFramePrivate;

#define IBG_TYPE_FRAME                  (ibg_frame_get_type ())
#define IBG_FRAME(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_FRAME, IbgFrame))
#define IBG_IS_FRAME(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_FRAME))
#define IBG_FRAME_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_FRAME, IbgFrameClass))
#define IBG_IS_FRAME_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_FRAME))
#define IBG_FRAME_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_FRAME, IbgFrameClass))

struct _IbgFrame
{
    GObject parent_instance;

    IbgFramePrivate *priv;
};

struct _IbgFrameClass
{
    GObjectClass parent_class;
};

GType
ibg_frame_get_type (void);

IbgFrame *
ibg_frame_new (IbgFrameConfig *config);

AVFrame *
ibg_frame_alloc_raw_frame (enum AVPixelFormat pix_fmt, int width, int height);

IbgFrame *
ibg_frame_new_from_native (IbgFrameConfig *config, AVFrame *source, gboolean owns);

IbgFrameConfig *
ibg_frame_get_config (IbgFrame *frame);

int
ibg_frame_copy_raw_frame (IbgFrame *frame, const AVFrame *source);

int
ibg_frame_copy_frame (IbgFrame *frame, const IbgFrame *source);

AVFrame *
ibg_frame_get_frame (const IbgFrame *frame);

void
ibg_frame_get_dimensions (const IbgFrame *frame, guint32 *width, guint32 *height);

AVFrame *
ibg_frame_scale_raw_frame (AVFrame *input_frame, guint32 width, guint32 height);

G_END_DECLS

#endif
