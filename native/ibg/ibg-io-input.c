#include <ibg-io-input.h>
#include <ibg-frame.h>
#include <ibg-utils.h>

#define NATIVE_BUFFER_SIZE      4096

struct _IbgIoInputPrivate {
    IbgReadFunc read_func;
    int8_t is_eof;
};

static void
ibg_io_input_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgIoInput, ibg_io_input, IBG_TYPE_IO)

static void
ibg_io_input_class_init (IbgIoInputClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_io_input_finalize;
}

static void
ibg_io_input_init (IbgIoInput *self)
{
    self->priv = ibg_io_input_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_io_input_finalize (GObject *object)
{
    IbgIoInput *io = IBG_IO_INPUT (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)io);

    (*G_OBJECT_CLASS (ibg_io_input_parent_class)->finalize) (object);
}

static int
read_io_context (void *opaque, uint8_t *buf, int buf_size)
{
    IbgIoInput *context = IBG_IO_INPUT (opaque);
    gboolean enable_debug = ibg_io_enable_debug (IBG_IO (context));
    int ret;

    if (enable_debug)
        g_message (G_STRLOC ": %s: %p - %p - %d", G_STRFUNC, (void*)context, buf, buf_size);

    if (context->priv->is_eof) {
        if (enable_debug)
            g_message (G_STRLOC ": %s: %p - ALREADY EOF", G_STRFUNC, (void*)context);
        return AVERROR_EOF;
    }

    ret = context->priv->read_func (buf, buf_size);

    if (enable_debug)
        g_message (G_STRLOC ": %s: %p - %d", G_STRFUNC, (void*)context, ret);

    if (ret == 0) {
        if (enable_debug)
            g_message (G_STRLOC ": %s: %p - EOF", G_STRFUNC, (void*)context);
        context->priv->is_eof = 1;
        return AVERROR_EOF;
    }

    return ret;
}

IbgIoInput *
ibg_io_input_new (IbgReadFunc read_func, int8_t enable_debug)
{
    IbgIoInput *context;
    IbgIoClass *klass;
    unsigned char *buffer;
    AVIOContext *native_ctx;

    g_message (G_STRLOC ": %s", G_STRFUNC);

    context = g_object_new (IBG_TYPE_IO_INPUT, NULL);
    if (!context) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    buffer = av_malloc (NATIVE_BUFFER_SIZE);
    if (!buffer) {
        g_object_unref (context);
        return NULL;
    }

    context->priv->read_func = read_func;

    native_ctx = avio_alloc_context (buffer, NATIVE_BUFFER_SIZE, 0, context, read_io_context, NULL, NULL);

    g_message (G_STRLOC ": got io context %p", (void*)native_ctx);

    if (!native_ctx) {
        av_free (buffer);
        g_object_unref (context);
        return NULL;
    }

    klass = IBG_IO_CLASS (IBG_IO_INPUT_GET_CLASS (context));
    (* klass->initialize_context) (IBG_IO (context), native_ctx, enable_debug);

    return context;
}

IbgIoInput *
ibg_io_input_new_from_buffer (const void *input_buffer, int32_t size)
{
    IbgIoInput *context;
    AVIOContext *native_ctx;
    unsigned char *buffer;
    IbgIoClass *klass;

    g_message (G_STRLOC ": %s", G_STRFUNC);

    context = g_object_new (IBG_TYPE_IO_INPUT, NULL);
    if (!context)
        return NULL;

    buffer = av_malloc (size);
    if (!buffer) {
        g_object_unref (context);
        return NULL;
    }

    memcpy (buffer, input_buffer, size);

    native_ctx = avio_alloc_context (buffer, size, 0, context, NULL, NULL, NULL);

    g_message (G_STRLOC ": got io context %p", (void*)native_ctx);

    if (!native_ctx) {
        av_free (buffer);
        g_object_unref (context);
        return NULL;
    }

    klass = IBG_IO_CLASS (IBG_IO_INPUT_GET_CLASS (context));
    (* klass->initialize_context) (IBG_IO (context), native_ctx, FALSE);

    return context;
}

static int
read_input_stream (AVIOContext *io, IbgFrame *output_frame)
{
    int ret, stream_index;
    AVInputFormat *fmt = NULL;
    AVFormatContext *fmt_ctx = NULL;
    AVCodecContext *codec_ctx = NULL;
    AVCodec *codec = NULL;
    AVStream *stream;
    AVFrame *frame, *temp_frame = NULL;
    guint32 width, height;
    AVPacket pkt = { NULL };

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)io);

    ret = av_probe_input_buffer2 (io, &fmt, NULL, NULL, 0, 0);

    g_message (G_STRLOC ": %d - %p", ret, (void*)fmt);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    fmt_ctx = avformat_alloc_context ();
    fmt_ctx->pb = io;

    ret = avformat_open_input (&fmt_ctx, NULL, fmt, NULL);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    g_message (G_STRLOC);

    ret = avformat_find_stream_info (fmt_ctx, NULL);

    g_message (G_STRLOC);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    av_dump_format (fmt_ctx, 0, NULL, 0);

    g_message (G_STRLOC);

    ret = av_find_best_stream (fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &codec, 0);

    g_message (G_STRLOC ": %d - %p", ret, (void*)codec);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    stream_index = ret;

    codec_ctx = avcodec_alloc_context3 (codec);
    if (!codec_ctx) {
        g_warning (G_STRLOC ": Failed to create video codec context.");
        return -1;
    }

    ret = avcodec_open2 (codec_ctx, codec, NULL);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    stream = fmt_ctx->streams[stream_index];

    ret = avcodec_parameters_to_context (codec_ctx, stream->codecpar);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    g_message (G_STRLOC ": %d,%d - %d", stream->codecpar->width, stream->codecpar->height, codec_ctx->pix_fmt);

    temp_frame = ibg_frame_alloc_raw_frame (codec_ctx->pix_fmt, stream->codecpar->width, stream->codecpar->height);

    if (!temp_frame) {
        g_warning (G_STRLOC ": Failed to allocate temp frame.");
        return -1;
    }

    ret = av_read_frame (fmt_ctx, &pkt);

    g_message (G_STRLOC ": %d", ret);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    ret = avcodec_send_packet (codec_ctx, &pkt);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    av_packet_unref (&pkt);

    ret = avcodec_receive_frame (codec_ctx, temp_frame);

    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    g_message (G_STRLOC ": %p", (void*)output_frame);

    ibg_frame_get_dimensions (output_frame, &width, &height);

    frame = ibg_frame_scale_raw_frame (temp_frame, width, height);

    av_frame_free (&temp_frame);

    if (!frame) {
        g_warning (G_STRLOC ": Failed to scale image.");
        return -1;
    }

    ret = ibg_frame_copy_raw_frame (output_frame, frame);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    g_message (G_STRLOC ": %s: %p - DONE", G_STRFUNC, (void*)frame);

    av_frame_free (&frame);

    if (codec_ctx)
        avcodec_free_context (&codec_ctx);
    if (fmt_ctx)
        avformat_close_input (&fmt_ctx);

    return 0;
}

IbgIoInput *
ibg_io_input_new_from_image (HaskellVipsImage *image)
{
    IbgIoInput *context;
    void *buffer = NULL;
    size_t len = 0;
    int ret;

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)image, (void*)image->image);

    ret = haskell_vips_pngsave (image, &buffer, &len);
    if (ret < 0) {
        g_warning (G_STRLOC ": %s: failed to export image to PNG.", G_STRFUNC);
        return NULL;
    }

    g_message (G_STRLOC ": %p - %ld", buffer, len);

    context = ibg_io_input_new_from_buffer (buffer, (int32_t)len);

    g_message (G_STRLOC);

    return context;
}

int
ibg_io_input_read_stream (IbgFrame *frame, IbgIoInput *io)
{
    int ret;

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)frame, (void*)io);

    ret = read_input_stream (ibg_io_get_context (IBG_IO (io)), frame);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    g_message (G_STRLOC ": %s done: %p - %p", G_STRFUNC, (void*)frame, (void*)io);

    return ret;
}

int
ibg_io_input_flush_stream (IbgIoInput *io)
{
    unsigned char buffer[BUFSIZ];
    int ret;

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)io);

    do {
        g_message (G_STRLOC);
        ret = avio_read (ibg_io_get_context (IBG_IO (io)), buffer, BUFSIZ);
        g_message (G_STRLOC ": %d", ret);

        if (ret < 0 && ret != AVERROR_EOF) {
            ibg_print_error (G_STRLOC, ret);
            return ret;
        }
    } while (ret > 0);

    return 0;
}
