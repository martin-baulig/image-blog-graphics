#ifndef __IBG_IO_INPUT_H__
#define __IBG_IO_INPUT_H__ 1

#include <ibg-io.h>

#include <haskell-vips.h>

G_BEGIN_DECLS

typedef struct _IbgIoInput                 IbgIoInput;
typedef struct _IbgIoInputClass            IbgIoInputClass;
typedef struct _IbgIoInputPrivate          IbgIoInputPrivate;

#define IBG_TYPE_IO_INPUT                  (ibg_io_input_get_type ())
#define IBG_IO_INPUT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_IO_INPUT, IbgIoInput))
#define IBG_IS_IO_INPUT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_IO_INPUT))
#define IBG_IO_INPUT_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_IO_INPUT, IbgIoInputClass))
#define IBG_IS_IO_INPUT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_IO_INPUT))
#define IBG_IO_INPUT_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_IO_INPUT, IbgIoInputClass))

struct _IbgIoInput
{
    IbgIo parent_instance;

    IbgIoInputPrivate *priv;
};

struct _IbgIoInputClass
{
    IbgIoClass parent_class;
};

typedef int (*IbgReadFunc)(uint8_t *, int);

GType
ibg_io_input_get_type (void);

IbgIoInput *
ibg_io_input_new (IbgReadFunc read_func, int8_t enable_debug);

IbgIoInput *
ibg_io_input_new_from_buffer (const void *input_buffer, int32_t size);

IbgIoInput *
ibg_io_input_new_from_image (HaskellVipsImage *image);

G_END_DECLS

#endif
