#include <ibg-io-output.h>
#include <ibg-utils.h>

struct _IbgIoOutputPrivate {
    IbgWriteFunc write_func;
};

static void
ibg_io_output_finalize (GObject *object);

#define NATIVE_BUFFER_SIZE      4096

G_DEFINE_TYPE_WITH_PRIVATE (IbgIoOutput, ibg_io_output, IBG_TYPE_IO)

static void
ibg_io_output_class_init (IbgIoOutputClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_io_output_finalize;
}

static void
ibg_io_output_init (IbgIoOutput *self)
{
    self->priv = ibg_io_output_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_io_output_finalize (GObject *object)
{
    IbgIoOutput *io = IBG_IO_OUTPUT (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)io);

    (*G_OBJECT_CLASS (ibg_io_output_parent_class)->finalize) (object);
}

static int
write_io_context (void *opaque, uint8_t *buf, int buf_size)
{
    IbgIoOutput *context = IBG_IO_OUTPUT (opaque);
    gboolean enable_debug = ibg_io_enable_debug (IBG_IO (context));

    if (enable_debug)
        g_message (G_STRLOC ": %s: %p - %p - %d", G_STRFUNC, (void*)context, buf, buf_size);

    context->priv->write_func (buf, buf_size);

    if (enable_debug)
        g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)context);

    return buf_size;
}

IbgIoOutput *
ibg_io_output_new (IbgWriteFunc write_func, int8_t enable_debug)
{
    IbgIoOutput *context;
    unsigned char *buffer;
    AVIOContext *native_ctx;
    IbgIoClass *klass;

    context = g_object_new (IBG_TYPE_IO_OUTPUT, NULL);
    if (!context) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)context, (void*)context->priv);

    buffer = av_malloc (NATIVE_BUFFER_SIZE);
    if (!buffer) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        g_object_unref (context);
        return NULL;
    }

    context->priv->write_func = write_func;

    native_ctx = avio_alloc_context (buffer, NATIVE_BUFFER_SIZE, 1, context, NULL, write_io_context, NULL);

    g_message (G_STRLOC ": got io context %p", (void*)native_ctx);

    if (!native_ctx) {
        av_free (buffer);
        g_object_unref (context);
        return NULL;
    }

    klass = IBG_IO_CLASS (IBG_IO_OUTPUT_GET_CLASS (context));
    (* klass->initialize_context) (IBG_IO (context), native_ctx, enable_debug);

    return context;
}

int
ibg_io_output_begin_write (IbgIoOutput *io, IbgContext *context)
{
    return ibg_context_begin_write (context, io);
}
