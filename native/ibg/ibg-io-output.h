#ifndef __IBG_IO_OUTPUT_H__
#define __IBG_IO_OUTPUT_H__ 1

#include <ibg-io.h>

G_BEGIN_DECLS

typedef struct _IbgIoOutput                 IbgIoOutput;
typedef struct _IbgIoOutputClass            IbgIoOutputClass;
typedef struct _IbgIoOutputPrivate          IbgIoOutputPrivate;

#define IBG_TYPE_IO_OUTPUT                  (ibg_io_output_get_type ())
#define IBG_IO_OUTPUT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_IO_OUTPUT, IbgIoOutput))
#define IBG_IS_IO_OUTPUT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_IO_OUTPUT))
#define IBG_IO_OUTPUT_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_IO_OUTPUT, IbgIoOutputClass))
#define IBG_IS_IO_OUTPUT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_IO_OUTPUT))
#define IBG_IO_OUTPUT_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_IO_OUTPUT, IbgIoOutputClass))

struct _IbgIoOutput
{
    IbgIo parent_instance;

    IbgIoOutputPrivate *priv;
};

struct _IbgIoOutputClass
{
    IbgIoClass parent_class;
};

typedef void (*IbgWriteFunc)(uint8_t *, int);

GType
ibg_io_output_get_type (void);

IbgIoOutput *
ibg_io_output_new (IbgWriteFunc write_func, int8_t enable_debug);

int
ibg_io_output_begin_write (IbgIoOutput *io, IbgContext *context);

G_END_DECLS

#endif
