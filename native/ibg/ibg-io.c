#include <ibg-io.h>
#include <ibg-utils.h>

struct _IbgIoPrivate {
    AVIOContext *context;
    int8_t enable_debug;
};

static void
ibg_io_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgIo, ibg_io, G_TYPE_OBJECT)

static void
ibg_io_init_context (IbgIo *self, AVIOContext *context, gboolean enable_debug)
{
    if (self->priv->context)
        g_error (G_STRLOC ": %s: duplicate initialization.", G_STRFUNC);

    self->priv->context = context;
    self->priv->enable_debug = enable_debug;
}

static void
ibg_io_class_init (IbgIoClass *klass)
{
    GObjectClass   *gobject_class;

    klass->initialize_context = ibg_io_init_context;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_io_finalize;
}

static void
ibg_io_init (IbgIo *self)
{
    self->priv = ibg_io_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_io_finalize (GObject *object)
{
    IbgIo *io = IBG_IO (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)io);

    av_free (io->priv->context->buffer);
    av_free (io->priv->context);

    (*G_OBJECT_CLASS (ibg_io_parent_class)->finalize) (object);
}

IbgIo *
ibg_io_new (void)
{
    IbgIo *io;

    io = g_object_new (IBG_TYPE_IO, NULL);
    if (!io) {
        ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
        return NULL;
    }

    return io;
}

AVIOContext *
ibg_io_get_context (IbgIo *context)
{
    return context->priv->context;
}

gboolean
ibg_io_enable_debug (IbgIo *io)
{
    return io->priv->enable_debug;
}
