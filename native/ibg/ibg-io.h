#ifndef __IBG_IO_H__
#define __IBG_IO_H__ 1

#include <ibg-context.h>

G_BEGIN_DECLS

typedef struct _IbgIo                 IbgIo;
typedef struct _IbgIoClass            IbgIoClass;
typedef struct _IbgIoPrivate          IbgIoPrivate;

#define IBG_TYPE_IO                  (ibg_io_get_type ())
#define IBG_IO(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_IO, IbgIo))
#define IBG_IS_IO(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_IO))
#define IBG_IO_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_IO, IbgIoClass))
#define IBG_IS_IO_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_IO))
#define IBG_IO_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_IO, IbgIoClass))

struct _IbgIo
{
    GObject parent_instance;

    IbgIoPrivate *priv;
};

struct _IbgIoClass
{
    GObjectClass parent_class;

    void (*initialize_context)(IbgIo *self, AVIOContext *context, gboolean enable_debug);
};

GType
ibg_io_get_type (void);

IbgIo *
ibg_io_new (void);

AVIOContext *
ibg_io_get_context (IbgIo *context);

gboolean
ibg_io_enable_debug (IbgIo *io);

G_END_DECLS

#endif
