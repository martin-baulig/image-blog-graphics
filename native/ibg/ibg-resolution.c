#include <ibg-resolution.h>

int
ibg_get_resolution (IbgVideoResolution resolution, guint32 *width, guint32 *height)
{
    switch (resolution) {
    case IBG_VIDEO_RESOLUTION_LOW:
        *width = 640;
        *height = 360;
        return 0;
    case IBG_VIDEO_RESOLUTION_SD:
        *width = 854;
        *height = 480;
        return 0;
    case IBG_VIDEO_RESOLUTION_MIN_HD:
        *width = 1280;
        *height = 720;
        return 0;
    case IBG_VIDEO_RESOLUTION_HD:
        *width = 1920;
        *height = 1080;
        return 0;
    case IBG_VIDEO_RESOLUTION_4K:
        *width = 3840;
        *height = 2160;
        return 0;
    default:
        *width = *height = 0;
        return -1;
    }
}
