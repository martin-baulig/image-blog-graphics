#ifndef __IBG_RESOLUTION_H__
#define __IBG_RESOLUTION_H__ 1

#include <ibg-decls.h>

G_BEGIN_DECLS

typedef enum {
    IBG_VIDEO_RESOLUTION_DEFAULT,
    IBG_VIDEO_RESOLUTION_LOW,
    IBG_VIDEO_RESOLUTION_SD,
    IBG_VIDEO_RESOLUTION_MIN_HD,
    IBG_VIDEO_RESOLUTION_HD,
    IBG_VIDEO_RESOLUTION_4K
} IbgVideoResolution;

int
ibg_get_resolution (IbgVideoResolution resolution, guint32 *width, guint32 *height);

G_END_DECLS

#endif
