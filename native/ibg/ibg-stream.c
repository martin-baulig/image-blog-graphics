#include <ibg-stream.h>
#include <ibg-context.h>
#include <ibg-utils.h>

#include <libavutil/log.h>
#include <libavutil/timestamp.h>

struct _IbgStreamPrivate {
    IbgContext *context;
    AVCodec *codec;
    AVStream *stream;
    AVCodecContext *codec_context;
    AVFrame *frame;
    int64_t next_pts;
    guint32 width;
    guint32 height;
};

static void
ibg_stream_finalize (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (IbgStream, ibg_stream, G_TYPE_OBJECT)

static void
ibg_stream_class_init (IbgStreamClass *klass)
{
    GObjectClass   *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->finalize = ibg_stream_finalize;
}

static void
ibg_stream_init (IbgStream *self)
{
    self->priv = ibg_stream_get_instance_private (self);

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)self, (void*)self->priv);
}

static void
ibg_stream_finalize (GObject *object)
{
    IbgStream *stream = IBG_STREAM (object);

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)stream);

    (*G_OBJECT_CLASS (ibg_stream_parent_class)->finalize) (object);
}

static void
init_video_codec_context (AVCodecContext *c, guint32 width, guint32 height)
{
    c->codec_id = VIDEO_CODEC_ID;
    c->width    = width;
    c->height   = height;

    c->gop_size = 12;
    c->pix_fmt  = STREAM_PIX_FMT;
}

static void
set_video_stream_params (AVStream *st)
{
    AVCPBProperties *props;
    props = (AVCPBProperties*) av_stream_new_side_data(st, AV_PKT_DATA_CPB_PROPERTIES, sizeof(*props));
    props->buffer_size = STREAM_BUFFER_SIZE;
    props->max_bitrate = 0;
    props->min_bitrate = 0;
    props->avg_bitrate = 0;
    props->vbv_delay = UINT64_MAX;
}

static IbgStream *
alloc_video_stream (IbgContext *context, IbgFrameConfig *config)
{
    IbgStream *stream;

    stream = g_object_new (IBG_TYPE_STREAM, NULL);
    if (!stream)
        return NULL;

    stream->priv->context = context;

    stream->priv->codec = avcodec_find_encoder (VIDEO_CODEC_ID);
    if (!stream->priv->codec) {
        g_warning (G_STRLOC ": Cannot find codec.");
        g_object_unref (stream);
        return NULL;
    }

    stream->priv->stream = avformat_new_stream (ibg_context_get_context (context), stream->priv->codec);
    if (!stream->priv->stream) {
        g_warning (G_STRLOC ": Failed to create video stream.");
        g_object_unref (stream);
        return NULL;
    }

    g_message (G_STRLOC);

    stream->priv->stream->time_base = config->time_base;

    stream->priv->codec_context = avcodec_alloc_context3 (stream->priv->codec);
    if (!stream->priv->codec_context) {
        g_warning (G_STRLOC ": Failed to create video codec context.");
        g_object_unref (stream);
        return NULL;
    }

    stream->priv->codec_context->time_base = stream->priv->stream->time_base;

    stream->priv->width = config->width;
    stream->priv->height = config->height;

    init_video_codec_context (stream->priv->codec_context, config->width, config->height);

    set_video_stream_params (stream->priv->stream);

    return stream;
}

static int
open_video (IbgStream *stream)
{
    int ret;

    ret = avcodec_open2 (stream->priv->codec_context, stream->priv->codec, NULL);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    stream->priv->frame = ibg_frame_alloc_raw_frame (stream->priv->codec_context->pix_fmt,
                                                     stream->priv->codec_context->width,
                                                     stream->priv->codec_context->height);
    if (!stream->priv->frame)
        return -1;

    ret = avcodec_parameters_from_context (stream->priv->stream->codecpar, stream->priv->codec_context);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    return 0;
}

IbgStream *
ibg_stream_new_video (IbgContext *context, IbgIoOutput *io,
                      IbgFrameConfig *config)
{
    IbgStream *stream;

    g_message (G_STRLOC ": %s", G_STRFUNC);

    stream = alloc_video_stream (context, config);
    if (!stream)
        return NULL;

    if (open_video (stream) != 0) {
        g_object_unref (stream);
        return NULL;
    }

    g_message (G_STRLOC ": %p - %d", (void*)stream, stream->priv->codec_context->pix_fmt);

    return stream;
}

static int
write_encoded_frames (IbgStream *stream, int *got_frame)
{
    int ret = 0;

    g_message (G_STRLOC ": %s: %p", G_STRFUNC, (void*)stream);

    *got_frame = 0;

    while (ret >= 0) {
        AVPacket *pkt = av_packet_alloc ();
        if (!pkt) {
            ibg_print_error (G_STRLOC, AVERROR (ENOMEM));
            return AVERROR (ENOMEM);
        }

        ret = avcodec_receive_packet (stream->priv->codec_context, pkt);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            g_message (G_STRLOC ": %s again", G_STRFUNC);
            return ret;
        } else if (ret < 0) {
            ibg_print_error (G_STRLOC, ret);
            return ret;
        }

        ibg_log_packet (ibg_context_get_context (stream->priv->context), pkt);

        av_packet_rescale_ts (pkt, stream->priv->codec_context->time_base, stream->priv->stream->time_base);
        pkt->stream_index = stream->priv->stream->index;

        ret = av_interleaved_write_frame (ibg_context_get_context (stream->priv->context), pkt);

        if (ret < 0) {
            ibg_print_error (G_STRLOC, ret);
            return -1;
        }

        *got_frame = 1;
    }

    g_message (G_STRLOC ": %s: %ld - %d - %d", G_STRFUNC, stream->priv->frame->pts, *got_frame, ret);

    return ret;
}

int
ibg_stream_write_video_frame (IbgStream *stream, IbgFrame *frame)
{
    int ret, got_frame;

    g_message (G_STRLOC ": %s: %p - %p", G_STRFUNC, (void*)stream, (void*)frame);

    ret = av_frame_make_writable (stream->priv->frame);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

#if DEBUG
    g_message (G_STRLOC ": %p - %d, %d", (void*)frame, stream->priv->width, stream->priv->height);
#endif

    ret = av_frame_copy (stream->priv->frame, ibg_frame_get_frame (frame));
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    stream->priv->frame->pts = ++stream->priv->next_pts;
    stream->priv->next_pts += stream->priv->frame->nb_samples;

    ret = avcodec_send_frame (stream->priv->codec_context, stream->priv->frame);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        if (ret == AVERROR(EAGAIN)) {
            ret = write_encoded_frames (stream, &got_frame);
            g_message (G_STRLOC ": again: %d - %d", ret, got_frame);
        }
        return ret;
    }

    ret = write_encoded_frames (stream, &got_frame);
    if (ret != AVERROR(EAGAIN)) {
        g_message (G_STRLOC ": %s: got unexpected result %d", G_STRFUNC, ret);
        return ret;
    }

    g_message (G_STRLOC ": %s done: %p - %p - %d", G_STRFUNC, (void*)stream, (void*)frame, got_frame);

    return 0;
}

int
ibg_stream_finish_write (IbgStream *stream)
{
    int ret, got_frame;

    g_message (G_STRLOC ": %s", G_STRFUNC);

    ret = avcodec_send_frame (stream->priv->codec_context, NULL);
    if (ret < 0) {
        ibg_print_error (G_STRLOC, ret);
        return ret;
    }

    ret = write_encoded_frames (stream, &got_frame);
    if (ret != AVERROR_EOF) {
        g_message (G_STRLOC ": %s: got unexpected result %d", G_STRFUNC, ret);
        return ret;
    }

    g_message (G_STRLOC ": %d", got_frame);

    return 0;
}

AVFrame *
ibg_stream_alloc_raw_frame (IbgStream *stream)
{
    return ibg_frame_alloc_raw_frame (stream->priv->codec_context->pix_fmt,
                                      stream->priv->width, stream->priv->height);
}

IbgFrameConfig *
ibg_stream_get_frame_config (IbgStream *stream)
{
    return ibg_frame_config_new (stream->priv->codec_context->pix_fmt,
                                 stream->priv->width, stream->priv->height,
                                 stream->priv->codec_context->time_base,
                                 stream->priv->codec_context->sample_aspect_ratio);
}

AVCodecContext *
ibg_stream_get_codec_context (IbgStream *stream)
{
    return stream->priv->codec_context;
}
