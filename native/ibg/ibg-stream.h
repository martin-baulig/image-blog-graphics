#ifndef __IBG_STREAM_H__
#define __IBG_STREAM_H__ 1

#include <ibg-frame.h>
#include <ibg-context.h>
#include <ibg-resolution.h>
#include <ibg-io-output.h>

G_BEGIN_DECLS

#define STREAM_DURATION         10.0
#define VIDEO_CODEC_ID          AV_CODEC_ID_H265
#define STREAM_FRAME_RATE       25
#define STREAM_PIX_FMT          AV_PIX_FMT_YUV420P

#define STREAM_BUFFER_SIZE      (1024 * 1024 * 64)

#define OUTPUT_FORMAT           "mpeg"

typedef struct _IbgStream                 IbgStream;
typedef struct _IbgStreamClass            IbgStreamClass;
typedef struct _IbgStreamPrivate          IbgStreamPrivate;

#define IBG_TYPE_STREAM                  (ibg_stream_get_type ())
#define IBG_STREAM(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IBG_TYPE_STREAM, IbgStream))
#define IBG_IS_STREAM(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IBG_TYPE_STREAM))
#define IBG_STREAM_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), IBG_TYPE_STREAM, IbgStreamClass))
#define IBG_IS_STREAM_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), IBG_TYPE_STREAM))
#define IBG_STREAM_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), IBG_TYPE_STREAM, IbgStreamClass))

struct _IbgStream
{
    GObject parent_instance;

    IbgStreamPrivate *priv;
};

struct _IbgStreamClass
{
    GObjectClass parent_class;
};

GType
ibg_stream_get_type (void);

IbgStream *
ibg_stream_new (void);

IbgStream *
ibg_stream_new_video (IbgContext *context, IbgIoOutput *io,
                      IbgFrameConfig *config);

int
ibg_stream_write_video_frame (IbgStream *stream, IbgFrame *frame);

int
ibg_stream_finish_write (IbgStream *stream);

AVFrame *
ibg_stream_alloc_raw_frame (IbgStream *stream);

IbgFrameConfig *
ibg_stream_get_frame_config (IbgStream *stream);

AVCodecContext *
ibg_stream_get_codec_context (IbgStream *stream);

G_END_DECLS

#endif
