#include <ibg-utils.h>

#include <libavutil/log.h>
#include <libavutil/timestamp.h>

static volatile gint global_counter = 0;

GObject *
ibg_object_ref (GObject *obj)
{
    g_message (G_STRLOC ": %s - %p", G_STRFUNC, (void*)obj);
    return g_object_ref (obj);
}

void
ibg_object_unref (GObject *obj)
{
    g_message (G_STRLOC ": %s - %p", G_STRFUNC, (void*)obj);
    g_object_unref (obj);
}

void
ibg_print_error (const char *location, int err)
{
    char errbuf[BUFSIZ];
    const char *errbuf_ptr = errbuf;

    if (av_strerror (err, errbuf, sizeof (errbuf)) < 0)
        errbuf_ptr = strerror (AVUNERROR (err));

    g_warning ("%s: %s\n", location, errbuf_ptr);
}

void
ibg_log_packet (const AVFormatContext *fmt_ctx, const AVPacket *pkt)
{
    AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;

    g_message("pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s stream_index:%d\n",
              av_ts2str (pkt->pts), av_ts2timestr (pkt->pts, time_base),
              av_ts2str (pkt->dts), av_ts2timestr (pkt->dts, time_base),
              av_ts2str (pkt->duration), av_ts2timestr (pkt->duration, time_base),
              pkt->stream_index);
}

int
ibg_get_next_unique_id (void)
{
    return g_atomic_int_add (&global_counter, 1);
}
