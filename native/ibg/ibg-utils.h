#include <ibg-decls.h>

G_BEGIN_DECLS

GObject *
ibg_object_ref (GObject *obj);

void
ibg_object_unref (GObject *obj);

void
ibg_print_error (const char *location, int err);

void
ibg_log_packet (const AVFormatContext *fmt_ctx, const AVPacket *pkt);

int
ibg_get_next_unique_id (void);

G_END_DECLS
