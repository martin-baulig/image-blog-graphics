module ImageBlog.Graphics.Test.Main (main) where

import           Baulig.Prelude

import           ImageBlog.Graphics.Vips

import           Test.Tasty
import           Test.Tasty.HUnit

------------------------------------------------------------------------------
--- Vips Test
------------------------------------------------------------------------------

main :: IO ()
main = do
    initializeVips

    earth <- readFile "test/data/The_Blue_Marble_4463x4163.jpg"

    defaultMain
        $ testGroup "Vips"
                    [loadImage earth, thumbnailImage earth, resizeImage earth]

loadImage :: ByteString -> TestTree
loadImage file = testCase "Load Image" $ do
    runResourceT $ do
        image <- vipsImageFromBuffer file
        size <- vipsGetImageSize image
        lift $ do
            4463 @?= imageWidth size
            4163 @?= imageHeight size
            ImageSize 4463 4163 @=? size

thumbnailImage :: ByteString -> TestTree
thumbnailImage file = testCase "Thumbnail" $ do
    runResourceT $ do
        thumbnail <- vipsThumbnailBuffer file 80

        size <- vipsGetImageSize thumbnail
        lift $ ImageSize 80 75 @=? size

        out <- vipsWriteToMemory thumbnail

        lift $ 29854 @=? length out

resizeImage :: ByteString -> TestTree
resizeImage file = testCase "Resize image" $ runResourceT $ do
    image <- vipsImageFromBuffer file

    out <- runResourceT $ do
        thumbnail <- vipsThumbnailBuffer file 80

        size <- vipsGetImageSize thumbnail
        lift $ lift $ ImageSize 80 75 @=? size

        vipsWriteToMemory thumbnail

    lift $ 29854 @=? length out

    runResourceT $ do
        resized <- vipsResizeImageCopy image 0.2

        size <- vipsGetImageSize resized
        lift $ lift $ ImageSize 893 833 @=? size

        blob <- vipsWriteToMemory resized
        lift $ lift $ 135358 @=? length blob
